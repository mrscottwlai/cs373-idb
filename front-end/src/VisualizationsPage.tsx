import { Box, Typography, Divider } from '@mui/material';
import OurVisualizations from './OurVisualizations';
import ProviderVisualizations from './ProviderVisualizations';

const Visualizations = () => {
    return (
        <Box>
        <Typography id="visualizationsPageTitle" gutterBottom variant="h3" component="div" pt={2}>
          Visualizations
        </Typography>
        <OurVisualizations />
        <Divider style={{ width: '70%', margin: '0 auto' }} />
        <ProviderVisualizations />
        </Box>
    );
  };
  
  export default Visualizations;
  