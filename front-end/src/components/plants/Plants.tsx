import { CardActionArea, Box, List, ListItem, ListItemText, Divider, Typography, Card, CardContent, CardMedia, CssBaseline, Grid, Container } from '@mui/material';
import PlantInstanceGrid from "./PlantInstanceGrid"
function Plants() {
    return (
        <Box>
            <CssBaseline />
                    <Typography variant="h1" align="center" gutterBottom>
                        Plants
                    </Typography>
                    <hr/>
            <PlantInstanceGrid />



        </Box>
    );
}

export default Plants;
