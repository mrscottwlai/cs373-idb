/* eslint-disable no-undef */
import React from 'react';
import { render, screen } from '@testing-library/react';
import "@testing-library/jest-dom";
import { BrowserRouter } from "react-router-dom";
import ReactDOM from "react-dom";

import App from './App';
import Navigation from "./components/Navbar"
import Home from "./components/Home"
import About from './components/About';
import Animals from "./components/animals/Animals"
import Animal from "./components/animals/AnimalInstanceCard"
import Plants from "./components/plants/Plants"
import Plant from "./components/plants/PlantInstanceCard"
import States from "./components/states/States"
import State from "./components/states/StateInstanceCard"

describe("Render Basic Components", () => {
  // Test 1
  test('App Renders', () => {
    render(
      <BrowserRouter>
        <App />
      </BrowserRouter>
    );
    expect(
      screen.getByText(
        "The United States has more than 1300 species of plants and animals classified as endangered or at risk"
      )
    ).toBeInTheDocument();
  });

  // Test 2
  test("Home buttons are not disabled", () => {
    render(
      <BrowserRouter>
        <App />
      </BrowserRouter>
    );
    const buttonItems = screen.getAllByRole("button");
    buttonItems.forEach((button) => {
      expect(button).not.toBeDisabled();
    });
  });

  // Test 3
  test("All link buttons are not disabled", () => {
    render(
      <BrowserRouter>
        <App />
      </BrowserRouter>
    );
    const buttonItems = screen.getAllByRole("link");
    buttonItems.forEach((button) => {
      expect(button).not.toBeDisabled();
    });
  });

  // Test 4
  test("About renders names without crashing", () => {
    render(
      <BrowserRouter>
      <About />
      </BrowserRouter>
    );
    expect(screen.getByText("Scott Lai")).toBeInTheDocument();
    expect(screen.getByText("Colin Liu")).toBeInTheDocument();
    expect(screen.getByText("Victor Favela")).toBeInTheDocument();
    expect(screen.getByText("Brian Wan")).toBeInTheDocument();
    expect(screen.getByText("Lucas Zimmer")).toBeInTheDocument();
  });

  // Test 5
  test("About renders fully without crashing", () => {
    render(
      <BrowserRouter>
        <About />
      </BrowserRouter>
    );
    expect(screen.getByText('Tools and Frameworks')).toBeInTheDocument();
  });

  // Test 6
  test("Navbar renders without crashing", () => {
    render(
      <BrowserRouter>
        <Navigation/>
      </BrowserRouter>
    );
    expect(screen.getByText("Endangered Nature")).toBeInTheDocument();
  });

  // Test 7
  test("Animals renders without crashing", () => {
    render(
      <BrowserRouter>
        <Animals/>
      </BrowserRouter>
    );
    expect(screen.getByText("Animals")).toBeInTheDocument();
  });

  // Test 8
  test("Plants renders without crashing", () => {
    render(
      <BrowserRouter>
        <Plants/>
      </BrowserRouter>
    );
    expect(screen.getByText("Plants")).toBeInTheDocument();
  });

  // Test 9
  test("States renders without crashing", () => {
    render(
      <BrowserRouter>
        <States/>
      </BrowserRouter>
    );
    expect(screen.getByText("States")).toBeInTheDocument();
  });

  // Test 10
  test("Home renders without crashing", () => {
    render(
      <BrowserRouter>
        <Home/>
      </BrowserRouter>
    );
    expect(screen.getByText("The United States has more than 1300 species of plants and animals classified as endangered or at risk")).toBeInTheDocument();
  });

  // Search Test
  test("Sitewide searchbar renders without crashing", () => {
    render(
      <BrowserRouter>
        <Navigation/>
      </BrowserRouter>
    );
    expect(screen.getByTestId('sitewide-search')).toBeInTheDocument();
  });

  test("Model searchbar renders without crashing", () => {
    render(
      <BrowserRouter>
        <Animals/>
      </BrowserRouter>
    );
    expect(screen.getByTestId('search')).toBeInTheDocument();
  });

  // Filtering Test
  test("Filters render without crashing", () => {
    render(
      <BrowserRouter>
        <Plants/>
      </BrowserRouter>
    );
    expect(screen.getByTestId('filter')).toBeInTheDocument();
  });

  // Sorting Test
  test("Sorting renders without crashing", () => {
    render(
      <BrowserRouter>
        <States/>
      </BrowserRouter>
    );
    expect(screen.getByTestId('sort')).toBeInTheDocument();
  });

});