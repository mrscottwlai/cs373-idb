from flask import Blueprint
from .. import impl

bp = Blueprint("states", __name__)


@bp.route("/states", methods=["get"])
def GetStates():

    return impl.states.GetStates()


@bp.route("/states/<stateId>", methods=["get"])
def GetStateId(stateId):

    options = {}
    options["stateId"] = stateId

    return impl.states.GetStateId(options)
