from database import (
    db,
    State,
    Border,
    City,
    Industry,
    Animal,
    FoodHabit,
    Habitat,
    AnimalHabitat,
    PlantHabitat,
    Plant,
    EconomicUse,
    AnimalState,
    PlantState,
)

stateNameCache = {}


def create_cache(app):
    global stateNameCache
    with app.app_context():
        for i in range(1, 51):
            state = db.session.query(State).get(i)
            stateNameCache[i] = state.name
