from flask import Flask

import os

# from sqlalchemy import create_engine
# from sqlalchemy.orm import sessionmaker


from .api import animals
from .api import plants
from .api import states

# engine = create_engine("postgresql://user:pass@localhost:port/mydatabase")
# Session = sessionmaker(engine)


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(SECRET_KEY="dev")
    if test_config is None:
        app.config.from_pyfile("config.py", silent=True)
    else:
        app.config.from_mapping(test_config)

    try:
        print(f"Instance path = {app.instance_path}")
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # Add blueprints
    app.register_blueprint(animals.bp)
    app.register_blueprint(plants.bp)
    app.register_blueprint(states.bp)

    return app
