import { Typography, CssBaseline, Container } from '@mui/material';
import AnimalInstanceGrid from "./AnimalInstanceGrid"

function Animals() {
    return (
        <>
            <CssBaseline />
            <main>
                <Container>
                    <Typography variant="h1" align="center">
                        Animals
                    </Typography>
                </Container>
                <hr/>
                <AnimalInstanceGrid />
            </main>
        </>
    );
}

export default Animals;
