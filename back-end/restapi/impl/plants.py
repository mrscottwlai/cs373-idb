import json
import time
from database import (
    db,
    State,
    Border,
    City,
    Industry,
    Animal,
    FoodHabit,
    Habitat,
    AnimalHabitat,
    PlantHabitat,
    Plant,
    EconomicUse,
    AnimalState,
    PlantState,
)
from flask import request
from sqlalchemy.orm import selectinload
import common

# import cache


def PlantToDict(plant, details):
    dictionary = common.object_as_dict(plant)
    dictionary["states"] = [
        {"id": plantState.state_id, "name": plantState.state.name}
        for plantState in plant.states
    ]
    dictionary["economicUses"] = [economicUse.use for economicUse in plant.economicUses]
    dictionary["habitats"] = [habitat.name for habitat in plant.habitats]
    if details:
        visitedAnimals = set()
        dictionary["animals"] = []
        for plantState in plant.states:
            for animalState in plantState.state.animals:
                animal_id = animalState.animal_id
                if animal_id not in visitedAnimals:
                    visitedAnimals.add(animal_id)
                    dictionary["animals"].append(
                        {"id": animal_id, "name": animalState.animal.primaryCommonName}
                    )
    return dictionary


def GetPlants():
    startTime = time.time()
    # attributes selected from a dropdown
    exact_filter_fields = [
        Plant.taxonomyClass,
        Plant.category,
        Plant.duration,
        Plant.growthHabit,
        Plant.globalStatus,
        Plant.nationalStatus,
        Plant.threatLevel,
    ]
    # attributes to sort by
    sort_fields = [Plant.scientificName, Plant.primaryCommonName]
    # attributes to search by
    search_fields = [Plant.scientificName, Plant.primaryCommonName, Plant.nameCategory]
    # get dict of queries
    queries = request.args.to_dict(flat=False)
    # create BaseQuery
    plants = db.session.query(Plant).options(
        selectinload(Plant.states).selectinload(PlantState.state),
        selectinload(Plant.economicUses),
        selectinload(Plant.habitats),
    )
    # get page data from query
    page = common.get_query("page", queries)
    page = 1 if page == None else int(page[0])
    per_page = (
        int(common.get_query("perPage", queries).pop())
        if common.get_query("perPage", queries)
        else 20
    )
    # get filter data from query
    try:
        query_field = common.parse_request_param(request.args, "q", str)
        exact_filters = common.parse_exact_filters(
            request.args, Plant, exact_filter_fields
        )
        sort_logic = common.parse_sort_fields(
            request.args, Plant, sort_fields, Plant.primaryCommonName
        )
    except Exception as e:
        return {"status": "fail", "data": e.args}
    # apply search data to BaseQuery
    if query_field is not None:
        plants = common.add_search_filters(plants, search_fields, query_field)
    # apply filter data to BaseQuery
    if len(exact_filters) > 0:
        plants = common.add_exact_filters(plants, exact_filters)
    # apply sorting to BaseQuery
    if sort_logic is None:
        plants = plants.order_by(Plant.primaryCommonName.desc())
    else:
        plants = plants.order_by(sort_logic)
    # paginate using given page queries
    plant_page = plants.paginate(page=page, per_page=per_page)
    # get instances from just one of the pages
    result = plant_page.items
    # format to JSON
    common.printTimeDebug("/plants before ToDict", startTime)
    outputList = [PlantToDict(row, False) for row in result]
    # combine # of instances in query & a query page into a dict
    ret = {"count": plants.count(), "page": outputList}
    # print(ret)
    common.printTimeDebug("/plants", startTime)
    return json.dumps(ret, cls=common.DecimalEncoder, indent=4), 200


def GetPlantId(options):
    """
    :param options: A dictionary containing all the paramters for the Operations
        options["plantId"]: The unique identifier of the plant
    """
    startTime = time.time()
    plant = (
        db.session.query(Plant)
        .options(
            selectinload(Plant.states)
            .selectinload(PlantState.state)
            .selectinload(State.animals)
            .selectinload(AnimalState.animal),
            selectinload(Plant.economicUses),
            selectinload(Plant.habitats),
        )
        .get(options["plantId"])
    )
    outputDict = PlantToDict(plant, True)
    common.printTimeDebug("/plants/id", startTime)
    return json.dumps(outputDict, cls=common.DecimalEncoder, indent=4), 200
