import { useEffect, useState, MouseEventHandler } from 'react';
import {
  Box,
  Grid,
  InputLabel,
  Select,
  MenuItem,
  FormControl,
  Radio,
  ListItemText,
  Tooltip,
  IconButton,
  CircularProgress,
} from '@mui/material';
import ClearAllIcon from '@mui/icons-material/ClearAll';
import PlantModelCard from './PlantModelCard';
import Pagination from '../../Pagination';
import SearchBar from '../SearchBar';
import { getPlants } from '../../APIFunctions';
import { threatLevels, globalStatuses, categories, durations, plantClasses, sortOptions, nationalStatuses } from './plantFilterInfo'
import { getHighlightSearch } from '../HighlightSearch';

const ModelGrid = () => {
  const [plantsData, setPlantsData] = useState<any[]>([]);
  const [loaded, setLoaded] = useState(false);
  const [page, setPage] = useState(0);
  const [count, setCount] = useState(0);
  const [threatLevel, setThreatLevel] = useState('');
  const [plantClass, setPlantClass] = useState('');
  const [globalStatus, setGlobalStatus] = useState('');
  const [nationalStatus, setNationalStatus] = useState('');
  const [category, setCategory] = useState('');
  const [duration, setDuration] = useState('');
  const [sortOption, setSortOption] = useState('');
  const [query, setQuery] = useState('');
  const [queryBuffer, setQueryBuffer] = useState('');

  useEffect(() => {
    console.log("called useEffect")
    getPlants(page+1, threatLevel, globalStatus, category, duration, plantClass, sortOption, query, nationalStatus).then(
      (response: any) => {
        // should be the 25 plants max on this page
        setPlantsData(response['page']);
        console.log("plantsData = " + plantsData)
        console.log("response['page'] = " + response['page'])
        // total num of plants in database, used to calculate total number of pages
        setCount(response['count'])
        setLoaded(true);

      }
    );
  }, [page, threatLevel, globalStatus, category, duration, plantClass, sortOption, query, nationalStatus]);
  
  const handlePageChange = (event: any) => {
    setPage(event.target.value);
  }

  const handleThreatLevelChange = (event: any) => {
    setThreatLevel(event.target.value);
    setPage(0);
  }

  const handleGlobalStatusChange = (event: any) => {
    setGlobalStatus(event.target.value);
    setPage(0);
  }

  const handleNationalStatusChange = (event: any) => {
    setNationalStatus(event.target.value);
    setPage(0);
  }

  const handleCategoryChange = (event: any) => {
    setCategory(event.target.value);
    setPage(0);
  }

  const handleDurationChange = (event: any) => {
    setDuration(event.target.value);
    setPage(0);
  }
  const handlePlantClassChange = (event: any) => {
    setPlantClass(event.target.value);
    setPage(0);
  }
  const handleSortOptionChange = (event: any) => {
    setSortOption(event.target.value);
    setPage(0);
  }

  const handleClearAllClick: MouseEventHandler<HTMLButtonElement> = () => {
    setPage(0);
    setThreatLevel('');
    setGlobalStatus('');
    setNationalStatus('');
    setCategory('');
    setDuration('');
    setPlantClass('');
    setSortOption('');
    setQuery('');
  }

  const handleQueryChange = (event: any) => {
    const value = event.target.value;
    //console.log(value)
    if (value == '') {
      setQueryBuffer('');
    } else {
      setQueryBuffer(value);
    }
  };

  const updateQuery = () => {
    //console.log("Running update Query")
    setPage(0);
    setQuery(queryBuffer);
    //console.log("Animal Search: ", value)
  }

  return (
    <Box>
      <Grid
        container
        spacing={0}
        pb={3}
        direction="column"
        alignItems="center"
        justifyContent="center"
      >

        <Grid id="searchBar" item xs={3}>
          <SearchBar query={query} onChange={handleQueryChange} updateQuery={updateQuery}/>
        </Grid>

      <Grid item pl={2} xs={3}>
          <div
            style={{
              display: 'inline-flex',
              alignItems: 'center',
              flexWrap: 'wrap',
            }}
          >
            <FormControl id="threatLevelFilter" sx={{ m: 1, width: 140 }} variant="standard">
              <InputLabel>Threat Level</InputLabel>
              <Select
                data-testid="filter"
                defaultValue=""
                value={threatLevel}
                onChange={handleThreatLevelChange}
                renderValue={(selected) => threatLevels.find((i) => i.value === selected)?.name}
              >
                {threatLevels.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    <Radio checked={threatLevel === element.value} />
                    <ListItemText primary={element.name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl sx={{ m: 1, width: 140 }} variant="standard">
              <InputLabel>Global Status</InputLabel>
              <Select
                defaultValue=""
                value={globalStatus}
                onChange={handleGlobalStatusChange}
                renderValue={(selected) => globalStatuses.find((i) => i.value === selected)?.name}
              >
                {globalStatuses.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    <Radio checked={globalStatus == element.value} />
                    <ListItemText primary={element.name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl sx={{ m: 1, width: 140 }} variant="standard">
              <InputLabel>National Status</InputLabel>
              <Select
                defaultValue=""
                value={nationalStatus}
                onChange={handleNationalStatusChange}
                renderValue={(selected) => nationalStatuses.find((i) => i.value === selected)?.name}
              >
                {nationalStatuses.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    <Radio checked={nationalStatus == element.value} />
                    <ListItemText primary={element.name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl sx={{ m: 1, width: 140 }} variant="standard">
              <InputLabel>Class</InputLabel>
              <Select
                defaultValue=""
                value={plantClass}
                onChange={handlePlantClassChange}
                renderValue={(selected) => plantClasses.find((i) => i.value === selected)?.name}
              >
                {plantClasses.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    <Radio checked={plantClass == element.value} />
                    <ListItemText primary={element.name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl sx={{ m: 1, width: 140 }} variant="standard">
              <InputLabel>Duration</InputLabel>
              <Select
                defaultValue=""
                value={duration}
                onChange={handleDurationChange}
                renderValue={(selected) => durations.find((i) => i.value === selected)?.name}
              >
                {durations.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    <Radio checked={duration == element.value} />
                    <ListItemText primary={element.name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl sx={{ m: 1, width: 140 }} variant="standard">
              <InputLabel>Category</InputLabel>
              <Select
                defaultValue=""
                value={category}
                onChange={handleCategoryChange}
                renderValue={(selected) => categories.find((i) => i.value === selected)?.name}
              >
                {categories.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    <Radio checked={category == element.value} />
                    <ListItemText primary={element.name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl id="sortMenu" sx={{ m: 1, width: 130 }} variant="standard">
              <InputLabel>Sort by</InputLabel>
              <Select
                data-testid="sort"
                defaultValue=""
                value={sortOption}
                onChange={handleSortOptionChange}
                renderValue={(selected) => sortOptions.find((i) => i.value === selected)?.name}
              >
                {sortOptions.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    {element.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <div style={{ paddingTop: 12 }}>
              <Tooltip title="Clear all">
                <IconButton aria-label="clear-all" onClick={handleClearAllClick}>
                  <ClearAllIcon />
                </IconButton>
              </Tooltip>
            </div>

          </div>






      </Grid>

      {loaded ? (
        <Grid
          container
          spacing={{ xs: 2, md: 3 }}
          columns={{ xs: 2, sm: 8, md: 16 }}
          p={3}
          justifyContent="center"
        >
          {
            plantsData.map((plant, i) => (
              <PlantModelCard
                key={plant.id}
                plant_id={plant.id}
                primaryCommonName={plant.primaryCommonName}
                scientificName={plant.scientificName}
                threatLevel={plant.threatLevel}
                category={plant.category}
                duration={plant.duration}
                growthHabit={plant.growthHabit}
                image={plant.image}
                highlightSearch={getHighlightSearch(query)}
              />
            ))
          }



        </Grid>
      ) : (
        <CircularProgress />
      )}
        </Grid>
        <Grid item xs={12}>
        <Grid container alignItems="center" justifyContent="center">
          <Pagination page={page} setPage={setPage} count={count} onChange={handlePageChange} />
        </Grid>
      </Grid>

    </Box>
  );
};

export default ModelGrid;
