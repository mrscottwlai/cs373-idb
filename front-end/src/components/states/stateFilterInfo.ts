const majorIndustries = [
  {
    name: 'Accommodation and Food Services',
    value: 'Accommodation and Food Services',
  },
  {
    name: 'Finance and Insurance',
    value: 'Finance and Insurance',
  },
  {
    name: 'Construction',
    value: 'Construction',
  },
  {
    name: 'Other Services',
    value: 'Other Services',
  },
  {
    name: 'Administrative, Support, Waste Management, and Remediation Services',
    value: 'Administrative, Support, Waste Management, and Remediation Services',
  },
  {
    name: 'Health Care and Social Assistance',
    value: 'Health Care and Social Assistance',
  },
  {
    name: 'Wholesale Trade',
    value: 'Wholesale Trade',
  },
  {
    name: 'Real Estate, Rental, and Leasing',
    value: 'Real Estate, Rental, and Leasing',
  },
  {
    name: 'Professional, Scientific, and Technical Services',
    value: 'Professional, Scientific, and Technical Services',
  }
]

const sortOptions = [
  {
    name: 'Name (A-Z)',
    value: 'name-asc',
  },
  {
    name: 'Name (Z-A)',
    value: 'name-desc',
  },
  {
    name: 'Land Area (Highest to Lowest)',
    value: 'landarea-desc',
  },
  {
    name: 'Land Area (Lowest to Highest)',
    value: 'landarea-asc',
  },
  {
    name: 'Water Area (Highest to Lowest)',
    value: 'waterarea-desc',
  },
  {
    name: 'Water Area (Lowest to Highest)',
    value: 'waterarea-asc',
  },
  {
    name: 'Educational Attainment (Highest to Lowest)',
    value: 'education-desc',
  },
  {
    name: 'Educational Attainment (Lowest to Highest)',
    value: 'education-asc',
  },
  {
    name: 'Business Number (Highest to Lowest)',
    value: 'business-desc',
  },
  {
    name: 'Business Number (Lowest to Highest)',
    value: 'business-asc',
  }
];

export {majorIndustries, sortOptions};
