import { useEffect, useState, MouseEventHandler } from 'react';
import {
  Box,
  Grid,
  InputLabel,
  Select,
  MenuItem,
  FormControl,
  Radio,
  ListItemText,
  Tooltip,
  IconButton,
  CircularProgress,
} from '@mui/material';
import ClearAllIcon from '@mui/icons-material/ClearAll';
import AnimalModelCard from './AnimalModelCard';
import { getAnimals } from '../../APIFunctions';
import Pagination from '../../Pagination';
import SearchBar from '../SearchBar';
import {threatLevels, globalStatuses, animalClasses, nationalStatuses, sortOptions } from './animalFilterInfo'
import { getHighlightSearch } from '../HighlightSearch';

const ModelGrid = () => {
  const [animalsData, setAnimalsData] = useState<any[]>([]);
  const [loaded, setLoaded] = useState(false);
  const [page, setPage] = useState(0);
  const [count, setCount] = useState(0);
  const [threatLevel, setThreatLevel] = useState('');
  const [animalClass, setAnimalClass] = useState('');
  const [globalStatus, setGlobalStatus] = useState('');
  const [nationalStatus, setNationalStatus] = useState('');
  const [sortOption, setSortOption] = useState('');
  const [query, setQuery] = useState('');
  const [queryBuffer, setQueryBuffer] = useState('');

  // runs the function () => {...} on 1st render & any time the dependences [...] change
  useEffect(() => {
    getAnimals(page+1, threatLevel, globalStatus, animalClass, sortOption, query, nationalStatus).then(
      (response: any) => {
        setAnimalsData(response['page']);
        setCount(response['count'])
        setLoaded(true);

      }
    );
  }, [page, threatLevel, globalStatus, animalClass, sortOption, query, nationalStatus]);

  const handlePageChange = (event: any) => {
    setPage(event.target.value);
  }

  const handleThreatLevelChange = (event: any) => {
    setThreatLevel(event.target.value);
    setPage(0);
  }

  const handleGlobalStatusChange = (event: any) => {
    setGlobalStatus(event.target.value);
    setPage(0);
  }

  const handleNationalStatusChange = (event: any) => {
    setNationalStatus(event.target.value);
    setPage(0);
  }

  const handleAnimalClassChange = (event: any) => {
    setAnimalClass(event.target.value);
    setPage(0);
  }
  const handleSortOptionChange = (event: any) => {
    setSortOption(event.target.value);
    setPage(0);
  }

  const handleClearAllClick: MouseEventHandler<HTMLButtonElement> = () => {
    setPage(0);
    setThreatLevel('');
    setGlobalStatus('');
    setNationalStatus('');
    setAnimalClass('');
    setSortOption('');
    setQuery('');
  }

  const handleQueryChange = (event: any) => {
    const value = event.target.value;
    if (value == '') {
      setQueryBuffer('');
    } else {
      setQueryBuffer(value);
    }
  };

  const updateQuery = () => {
    setPage(0);
    setQuery(queryBuffer);
  }

  return (
    <Box>
      <Grid
        container
        spacing={0}
        pb={3}
        direction="column"
        alignItems="center"
        justifyContent="center"
      >
        <Grid id="searchBar" item xs={3}>
          <SearchBar query={query} onChange={handleQueryChange} updateQuery={updateQuery}/>
        </Grid>
        <Grid item pl={2} xs={3}>
          <div
            style={{
              display: 'inline-flex',
              alignItems: 'center',
              flexWrap: 'wrap',
            }}
          >
            <FormControl id="threatLevelFilter" sx={{ m: 1, width: 140 }} variant="standard">
              <InputLabel>Threat Level</InputLabel>
              {/* form for user to slect a threat level */}
              <Select
                data-testid="filter"
                defaultValue=""
                value={threatLevel}
                onChange={handleThreatLevelChange}
                renderValue={(selected) => threatLevels.find((i) => i.value === selected)?.name}
              >
                {threatLevels.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    <Radio checked={threatLevel === element.value} />
                    <ListItemText primary={element.name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl sx={{ m: 1, width: 140 }} variant="standard">
              <InputLabel>Global Status</InputLabel>
              <Select
                defaultValue=""
                value={globalStatus}
                onChange={handleGlobalStatusChange}
                renderValue={(selected) => globalStatuses.find((i) => i.value === selected)?.name}
              >
                {globalStatuses.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    <Radio checked={globalStatus == element.value} />
                    <ListItemText primary={element.name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl sx={{ m: 1, width: 140 }} variant="standard">
              <InputLabel>National Status</InputLabel>
              <Select
                defaultValue=""
                value={nationalStatus}
                onChange={handleNationalStatusChange}
                renderValue={(selected) => nationalStatuses.find((i) => i.value === selected)?.name}
              >
                {nationalStatuses.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    <Radio checked={nationalStatus == element.value} />
                    <ListItemText primary={element.name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl sx={{ m: 1, width: 140 }} variant="standard">
              <InputLabel>Animal Class</InputLabel>
              <Select
                defaultValue=""
                value={animalClass}
                onChange={handleAnimalClassChange}
                renderValue={(selected) => animalClasses.find((i) => i.value === selected)?.name}
              >
                {animalClasses.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    <Radio checked={animalClass == element.value} />
                    <ListItemText primary={element.name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl id="sortMenu" sx={{ m: 1, width: 130 }} variant="standard">
              <InputLabel>Sort by</InputLabel>
              <Select
                data-testid="sort"
                defaultValue=""
                value={sortOption}
                onChange={handleSortOptionChange}
                renderValue={(selected) => sortOptions.find((i) => i.value === selected)?.name}
              >
                {sortOptions.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    {element.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <div style={{ paddingTop: 12 }}>
              <Tooltip title="Clear all">
                <IconButton aria-label="clear-all" onClick={handleClearAllClick}>
                  <ClearAllIcon />
                </IconButton>
              </Tooltip>
            </div>
          </div>
        </Grid>
        {loaded ? (
          <Grid
            container
            spacing={{ xs: 2, md: 3 }}
            columns={{ xs: 2, sm: 8, md: 16 }}
            p={3}
            justifyContent="center"
          >
            {
              animalsData.map((animal, i) => (
                <AnimalModelCard
                  key={animal.id}
                  animal_id={animal.id}
                  primaryCommonName={animal.primaryCommonName}
                  scientificName={animal.scientificName}
                  threatLevel={animal.threatLevel}
                  population={animal.population}
                  image={animal.image}
                  length={animal.length}
                  weight={animal.weight}
                  highlightSearch={getHighlightSearch(query)}
                />
              ))
            }
          </Grid>
        ) : (
          <CircularProgress />
        )}
      </Grid>
      <Grid item xs={12}>
        <Grid container alignItems="center" justifyContent="center">
          <Pagination page={page} setPage={setPage} count={count} onChange={handlePageChange} />
        </Grid>
      </Grid>
    </Box>
  );
};

export default ModelGrid;
