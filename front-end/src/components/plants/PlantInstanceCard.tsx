import { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import {
  Typography,
  Box,
  Container,
  Card,
  CardContent,
  CardMedia,
  CardActionArea,
  CircularProgress,
  List,
  ListItem,
  ListItemText,
  styled,
  Paper
} from '@mui/material';
import { Row } from 'react-bootstrap';
import { getPlant } from '../../APIFunctions';

const StyledCardActionArea = styled(CardActionArea)(({theme}) => `
    .MuiCardActionArea-focusHighlight {
        background: transparent;
    }
`);

const Plant = () => {
  const plant_id = useParams().id || '';

  const [plantData, setPlantData] = useState<any>([]);
  const [loadedData, setLoadedData] = useState(false);

  useEffect(() => {
    getPlant(plant_id).then((response: any) => {
      setPlantData(response);
      setLoadedData(true);
    });
  }, [plant_id]);

  let content = null;
  if (loadedData) {
    content = (
      <div className="About">
        <Container>
        <Row sx={{paddingBottom: '10%'}}>
          <Box sx={{ width: '35%' }}>
            <div className="DivLeft">
              <Typography sx={{ fontWeight: 'bold', fontSize: 'h2.fontSize'}}>{plantData.primaryCommonName}</Typography>
              <Typography sx={{ fontWeight: 'medium', fontSize: 'h4.fontSize', fontStyle: 'italic', paddingBottom:2 }}>{plantData.scientificName}</Typography>
            </div>


          </Box>

          <Box sx={{ width: '65%', paddingTop: '2%'}}>
              <div className="DivRight">
              <Typography sx={{ fontWeight: 'bold', fontSize: 'h3.fontSize'}}>Threat Level: {plantData.threatLevel}</Typography>
            </div>
          </Box>
        </Row>
        <hr/>
         <Row sx={{padding:'10%'}}>
          <Box sx={{ width: '100%', padding: '7%'}}>

            <Card variant="outlined">
              <CardActionArea style={{ outline: 'none' }}>
                <td dangerouslySetInnerHTML={{__html: plantData.video}} />
              </CardActionArea>
            </Card>
          </Box>
        </Row>
        <hr/>
        <Row>
          <Box sx={{width:'50%', paddingTop:'8%'}}>
            <Card variant="outlined">
              <StyledCardActionArea style={{ outline: 'none' }}>
                <CardContent sx={{ textAlign: 'left', }}>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h3.fontSize'}}>Plant Info</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Category: {plantData.category}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Duration: {plantData.duration}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Growth Habit: {plantData.growthHabit}</Typography>
                </CardContent>
              </StyledCardActionArea>
            </Card>
            <Card variant="outlined">
              <StyledCardActionArea style={{ outline: 'none' }}>
                <CardContent sx={{ textAlign: 'left', }}>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h3.fontSize'}}>Classification</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Name Category: {plantData.nameCategory}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Kingdom: {plantData.taxonomyKingdom}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Phylum: {plantData.taxonomyPhylum}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Class: {plantData.taxonomyClass}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Order: {plantData.taxonomyOrder}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Family: {plantData.taxonomyFamily}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Genus: {plantData.taxonomyGenus}</Typography>
                </CardContent>
              </StyledCardActionArea>
            </Card>

            <Card variant="outlined">
              <StyledCardActionArea style={{ outline: 'none' }}>
                <CardContent sx={{ textAlign: 'left', }}>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h3.fontSize'}}>Plant Status</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Threat Level: {plantData.threatLevel}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Global Status: {plantData.globalStatus}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>National Status: {plantData.nationalStatus}</Typography>
                </CardContent>
              </StyledCardActionArea>
            </Card>
          </Box>
          <Box sx={{width:'50%', paddingTop:'8%'}}>
            <Card variant="outlined">
              <StyledCardActionArea style={{ outline: 'none' }}>
                <CardMedia component="img" image={plantData.image}  />
              </StyledCardActionArea>
            </Card>
          </Box>

        </Row>
        <Row>
          <p> </p>
        </Row>
        <br />


        <Row padding="5">
            <Box sx={{width:'50%'}}>
              <Card variant="outlined">
                <StyledCardActionArea style={{ outline: 'none' }}>
                  <div className="DivCenter">
                    Habitats
                    <ul>
                      {plantData.habitats.map((habitat: string, i: number) => {
                        return (
                        <Typography key={i} sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>{habitat}</Typography>
                        );
                      })}
                  </ul>
                  </div>
                </StyledCardActionArea>
              </Card>
              <Card variant="outlined">
                <StyledCardActionArea style={{ outline: 'none' }}>
                  <div className="DivCenter">
                    Economic Uses
                    <ul>
                      {plantData.economicUses.map((uses: string, i: number) => {
                        return (
                        <Typography key={i} sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>{uses}</Typography>
                        );
                      })}
                  </ul>
                  </div>
                </StyledCardActionArea>
              </Card>
            </Box>
            <Box sx={{width:'50%'}}>
              <Card variant="outlined">
                <StyledCardActionArea style={{ outline: 'none' }}>
                  <div className="DivCenter">
                    States
                    <Paper style={{maxHeight: 200, overflow: 'auto'}}>

                      <List>
                        {plantData.states.map((state: {name: string, id:string}, i: number) => {
                          return (
                            <ListItem key={i} button component={Link} to={"/states/"+state.id}>
                              <ListItemText primary={state.name} />
                            </ListItem>
                          );
                        })}
                      </List>
                    </Paper>

                  </div>
                </StyledCardActionArea>
              </Card>
              <Card variant="outlined">
                <StyledCardActionArea style={{ outline: 'none' }}>
                  <div className="DivCenter">
                    Animals in same state
                    <Paper style={{maxHeight: 200, overflow: 'auto'}}>
                      <List>
                        {plantData.animals.map((animal: {name: string, id:string}, i: number) => {
                          return (
                            <ListItem key={i} button component={Link} to={"/animals/"+animal.id}>
                              <ListItemText primary={animal.name} />
                            </ListItem>
                          );
                        })}
                      </List>
                    </Paper>

                  </div>
                </StyledCardActionArea>
              </Card>
            </Box>
        </Row>
        <br />



      </Container>
    </div>
    );
  }

  return <Box>{loadedData ? content : <CircularProgress />}</Box>;
};

export default Plant;
