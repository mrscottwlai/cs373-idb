import { useEffect, useState, MouseEventHandler } from 'react';
import {
  Box,
  Grid,
  InputLabel,
  Select,
  MenuItem,
  FormControl,
  Slider,
  Tooltip,
  IconButton,
  CircularProgress,
} from '@mui/material';
import ClearAllIcon from '@mui/icons-material/ClearAll';
import StateModelCard from './StateModelCard';
import Pagination from '../../Pagination';
import SearchBar from '../SearchBar';
import { getStates } from '../../APIFunctions';
import { sortOptions } from './stateFilterInfo'
import { getHighlightSearch } from '../HighlightSearch';

const ModelGrid = () => {
  const [statesData, setStatesData] = useState<any[]>([]);
  const [loaded, setLoaded] = useState(false);
  const [page, setPage] = useState(0);
  const [count, setCount] = useState(0);
  const [sortOption, setSortOption] = useState('');
  const [query, setQuery] = useState('');
  const [queryBuffer, setQueryBuffer] = useState('');
  const [population, setPopulation] = useState([0,40000000])

  useEffect(() => {
    getStates(page+1, sortOption, query, population).then(
      (response: any) => {
        // should be the 25 plants max on this page
        setStatesData(response['page']);
        // total num of plants in database, used to calculate total number of pages
        setCount(response['count'])
        setLoaded(true);
      }
    );
  }, [page, sortOption, query, population]);

  const handlePageChange = (event: any) => {
    setPage(event.target.value);
  }

  const handleSortOptionChange = (event: any) => {
    setSortOption(event.target.value);
    setPage(0);
  }

  const handleClearAllClick: MouseEventHandler<HTMLButtonElement> = () => {
    setPage(0);
    setSortOption('');
    setQuery('')
    setPopulation([0,40000000]);
  }

  const handleQueryChange = (event: any) => {
    const value = event.target.value;
    if (value == '') {
      setQueryBuffer('');
    } else {
      setQueryBuffer(value);
    }
  };

  const updateQuery = () => {
    setPage(0);
    setQuery(queryBuffer);
  }

  const handlePopChange = (event: any, newPopulation: any) => {
    setPopulation(newPopulation);
    setPage(0);
  }

  return (
    <Box>
      <Grid
        container
        spacing={0}
        pb={3}
        direction="column"
        alignItems="center"
        justifyContent="center"
      >
        <Grid id="searchBar" item xs={3}>
          <SearchBar query={query} onChange={handleQueryChange} updateQuery={updateQuery}/>
        </Grid>

        <Grid item pl={2} xs={3}>
          <div
            style={{
              display: 'inline-flex',
              alignItems: 'center',
              flexWrap: 'wrap',
            }}
          >
            <FormControl sx={{ m: 1, width: 400 }} variant="standard">
              <InputLabel>Population</InputLabel>
              <Slider
                getAriaLabel={() => 'Population range'}
                value={population}
                onChange={handlePopChange}
                valueLabelDisplay="auto"
                min={0}
                max={40000000}
                step={4000000}
              >
                
              </Slider>
            </FormControl>

            <FormControl id="sortMenu" sx={{ m: 1, width: 130 }} variant="standard">
              <InputLabel>Sort by</InputLabel>
              <Select
                data-testid="sort"
                defaultValue=""
                value={sortOption}
                onChange={handleSortOptionChange}
                renderValue={(selected) => sortOptions.find((i) => i.value === selected)?.name}
              >
                {sortOptions.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    {element.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <div style={{ paddingTop: 12 }}>
              <Tooltip title="Clear all">
                <IconButton aria-label="clear-all" onClick={handleClearAllClick}>
                  <ClearAllIcon />
                </IconButton>
              </Tooltip>
            </div>
          </div>
        </Grid>
        {loaded ? (
          <Grid
            container
            spacing={{ xs: 2, md: 3 }}
            columns={{ xs: 2, sm: 8, md: 16 }}
            p={3}
            justifyContent="center"
          >
            {
              statesData.map((state, i) => (
                <StateModelCard
                  key={state.id}
                  state_id={state.id}
                  name={state.name}
                  pop={state.population}
                  abbreviation={state.abbreviation}
                  educationalAttainment={state.educationalAttainment}
                  businessNumber={state.businessNumber}
                  landArea={state.landArea}
                  waterArea={state.waterArea}
                  image={state.image}
                  highlightSearch={getHighlightSearch(query)}
                />
              ))
            }
          </Grid>
        ) : (
          <CircularProgress />
        )}
      </Grid>
      <Grid item xs={12}>
        <Grid container alignItems="center" justifyContent="center">
          <Pagination page={page} setPage={setPage} count={count} onChange={handlePageChange} />
        </Grid>
      </Grid>

    </Box>
  );
};

export default ModelGrid;
