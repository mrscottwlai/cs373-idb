import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';

const SearchBar = (props: any) => {
    const { value, onChange , updateQuery} = props;
  
    const onKeyDown = (event: any) => {
      if (event.key === 'Enter') {
        //console.log("enter was pressed")
        updateQuery();
        event.preventDefault();
      }
    };

    const onClick = (event: any) => {
      console.log("search button was pressed")
      updateQuery();
      event.preventDefault();
    };
  
    return (
      <Paper
        component="form"
        sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 355 }}
      >
        <InputBase
          data-testid="search"
          sx={{ ml: 1, flex: 1 }}
          placeholder="Search"
          inputProps={{ 'aria-label': 'search' }}
          value={value}
          onChange={onChange}
          onKeyDown={onKeyDown}
        />
        <IconButton type="submit" sx={{ p: '10px' }} aria-label="search" onClick = {onClick}>
          <SearchIcon />
        </IconButton>
      </Paper>
    );
  };
  
  export default SearchBar;
  