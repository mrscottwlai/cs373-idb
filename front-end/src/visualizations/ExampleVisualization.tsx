import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Box, Typography, Grid, CircularProgress } from '@mui/material';
import { Bar, BarChart, CartesianGrid, XAxis, YAxis, Tooltip } from 'recharts';

const ExampleVisualization = () => {

    return (
        <Typography gutterBottom variant="subtitle1" component="div" pt={2}>
            Example Visualization
        </Typography>
    );
};

export default ExampleVisualization;