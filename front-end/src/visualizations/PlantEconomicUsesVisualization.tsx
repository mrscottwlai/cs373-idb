import { useEffect, useState } from 'react';
import { Box, Typography, Grid, CircularProgress } from '@mui/material';
import { PieChart, Pie, Sector, Cell, ResponsiveContainer, Tooltip, Legend, } from 'recharts';
import { getState } from '../APIFunctions';

const cached_data = [
    { name: 'Ornamental' , value: 116 },
	{ name: 'Landscaping' , value: 73 },
	{ name: 'Medicine' , value: 61 },
	{ name: 'Other' , value: 47 },
	{ name: 'Revegetation' , value: 37 },
	{ name: 'Cosmetics' , value: 31 },
	{ name: 'Showy wildflower' , value: 22 },
	{ name: 'Building materials' , value: 13 },
	{ name: 'Esthetic' , value: 12 },
	{ name: 'Fiber' , value: 11 },
	{ name: 'Folk medicine' , value: 10 },
	{ name: 'Food' , value: 10 },
	{ name: 'Forage' , value: 8 },
    { name: 'Erosion control' , value: 8 },
	{ name: 'Pasture' , value: 5 },
	{ name: 'Pharmaceutical' , value: 4 },
    { name: 'Other food' , value: 4 },
	{ name: 'Fruit' , value: 4 },
	{ name: 'Beverage alcoholic' , value: 3 },
    { name: 'Industrial' , value: 3 },
    { name: 'Windbreak' , value: 3 },
	{ name: 'Beverage non-alcoholic' , value: 2 },
]


const PlantEconomicUsesVisualization = () => {
    const [data, setData] = useState<any[]>([]);
    const [loaded, setLoaded] = useState(true);

    let renderLabel = function(entry:any) {
        return entry.name;
    }

    useEffect(() => {
        if (!loaded){
            const tempData: any[] = [];
        }
        else {
            setData(cached_data)
        }
    }, []);

    return (
        <Box>
            <Typography gutterBottom variant="subtitle1" component="div" pt={2}>
            Plant Economic Uses Makeup
            </Typography>
            <Grid container justifyContent="center">
                {loaded ? (
                    <PieChart width={1000} height={500}>
                        <Pie
                            data={data}
                            dataKey="value"
                            nameKey="name"
                            fill="#198754"
                            legendType = "line"
                            label = {renderLabel}
                        />
                        <Tooltip />
                  </PieChart>
        
                ) : (
                    <CircularProgress />
                )}
            </Grid>
        </Box>

    );
};

export default PlantEconomicUsesVisualization;