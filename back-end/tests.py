import unittest
from urllib import response
import requests


class TestFlaskBackend(unittest.TestCase):
    def setUp(self):
        self.headers = {"Accept": "application/vnd.api+json"}
        # self.headers = {} <- in case ^ doesnt work, not sure why its needed in the first place

        self.api_url = "http://api.endangerednature.me/"

    """
    def testBasicRoute(self):
        response = requests.get(self.api_url)
        self.assertEqual(response.status_code, 200)
    """

    def testStates(self):
        response = requests.get(self.api_url + "states", headers=self.headers)
        self.assertEqual(response.status_code, 200)
        response_json = response.json()
        self.assertEqual(len(response_json), 2)

    def testStateID(self):
        response = requests.get(self.api_url + "states/1", headers=self.headers)
        self.assertEqual(response.status_code, 200)
        response_json = response.json()
        self.assertEqual(response_json["id"], 1)

    def testAnimals(self):
        response = requests.get(self.api_url + "animals", headers=self.headers)
        self.assertEqual(response.status_code, 200)
        response_json = response.json()
        self.assertEqual(len(response_json), 2)

    def testAnimalID(self):
        response = requests.get(self.api_url + "animals/1", headers=self.headers)
        self.assertEqual(response.status_code, 200)
        response_json = response.json()
        self.assertEqual(response_json["id"], 1)

    def testPlants(self):
        response = requests.get(self.api_url + "plants", headers=self.headers)
        self.assertEqual(response.status_code, 200)
        response_json = response.json()
        self.assertEqual(len(response_json), 2)

    def testPlantID(self):
        response = requests.get(self.api_url + "plants/1", headers=self.headers)
        self.assertEqual(response.status_code, 200)
        response_json = response.json()
        self.assertEqual(response_json["id"], 1)

    def testdummyTest(self):
        # response = requests.get("APIURL", headers = self.headers)
        # self.assertEqual(response.status_code,200)
        # response_json = response.json()
        # self.assertEqual(len(response_json['data']),10)
        self.assertEqual(0, 0)


if __name__ == "__main__":
    unittest.main()
