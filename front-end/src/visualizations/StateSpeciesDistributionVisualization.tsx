import { useEffect, useState } from 'react';
import { Box, Typography, Grid, CircularProgress } from '@mui/material';
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import { getState } from '../APIFunctions';

const cached_data = [
    {
        name: 'Alaska',
        animals: 86,
        plants: 15,
        },
        {
        name: 'Alabama',
        animals: 145,
        plants: 51,
        },
        {
        name: 'Arkansas',
        animals: 130,
        plants: 36,
        },
        {
        name: 'Arizona',
        animals: 135,
        plants: 26,
        },
        {
        name: 'California',
        animals: 152,
        plants: 56,
        },
        {
        name: 'Colorado',
        animals: 139,
        plants: 30,
        },
        {
        name: 'Connecticut',
        animals: 101,
        plants: 34,
        },
        {
        name: 'Delaware',
        animals: 109,
        plants: 33,
        },
        {
        name: 'Florida',
        animals: 150,
        plants: 34,
        },
        {
        name: 'Georgia',
        animals: 144,
        plants: 57,
        },
        {
        name: 'Hawaii',
        animals: 25,
        plants: 8,
        },
        {
        name: 'Iowa',
        animals: 112,
        plants: 35,
        },
        {
        name: 'Idaho',
        animals: 95,
        plants: 26,
        },
        {
        name: 'Illinois',
        animals: 123,
        plants: 47,
        },
        {
        name: 'Indiana',
        animals: 119,
        plants: 44,
        },
        {
        name: 'Kansas',
        animals: 138,
        plants: 26,
        },
        {
        name: 'Kentucky',
        animals: 118,
        plants: 44,
        },
        {
        name: 'Louisiana',
        animals: 135,
        plants: 32,
        },
        {
        name: 'Massachusetts',
        animals: 114,
        plants: 37,
        },
        {
        name: 'Maryland',
        animals: 121,
        plants: 41,
        },
        {
        name: 'Maine',
        animals: 103,
        plants: 33,
        },
        {
        name: 'Michigan',
        animals: 108,
        plants: 45,
        },
        {
        name: 'Minnesota',
        animals: 120,
        plants: 32,
        },
        {
        name: 'Missouri',
        animals: 124,
        plants: 41,
        },
        {
        name: 'Mississippi',
        animals: 131,
        plants: 41,
        },
        {
        name: 'Montana',
        animals: 111,
        plants: 25,
        },
        {
        name: 'North Carolina',
        animals: 147,
        plants: 57,
        },
        {
        name: 'North Dakota',
        animals: 113,
        plants: 20,
        },
        {
        name: 'Nebraska',
        animals: 136,
        plants: 28,
        },
        {
        name: 'New Hampshire',
        animals: 100,
        plants: 31,
        },
        {
        name: 'New Jersey',
        animals: 124,
        plants: 35,
        },
        {
        name: 'New Mexico',
        animals: 137,
        plants: 29,
        },
        {
        name: 'Nevada',
        animals: 107,
        plants: 18,
        },
        {
        name: 'New York',
        animals: 119,
        plants: 45,
        },
        {
        name: 'Ohio',
        animals: 106,
        plants: 43,
        },
        {
        name: 'Oklahoma',
        animals: 151,
        plants: 34,
        },
        {
        name: 'Oregon',
        animals: 120,
        plants: 28,
        },
        {
        name: 'Pennsylvania',
        animals: 101,
        plants: 50,
        },
        {
        name: 'Rhode Island',
        animals: 101,
        plants: 30,
        },
        {
        name: 'South Carolina',
        animals: 138,
        plants: 49,
        },
        {
        name: 'South Dakota',
        animals: 133,
        plants: 28,
        },
        {
        name: 'Tennessee',
        animals: 117,
        plants: 55,
        },
        {
        name: 'Texas',
        animals: 186,
        plants: 41,
        },
        {
        name: 'Utah',
        animals: 109,
        plants: 29,
        },
        {
        name: 'Virginia',
        animals: 127,
        plants: 51,
        },
        {
        name: 'Vermont',
        animals: 91,
        plants: 31,
        },
        {
        name: 'Washington',
        animals: 116,
        plants: 27,
        },
        {
        name: 'Wisconsin',
        animals: 108,
        plants: 37,
        },
        {
        name: 'West Virginia',
        animals: 95,
        plants: 42,
        },
        {
        name: 'Wyoming',
        animals: 116,
        plants: 22,
        }
]

function addStateDistribution(stateData: any, tempData: any[]) {
    console.log(stateData);
    console.log("------");
}

const StateSpeciesDistributionVisualization = () => {
    const [data, setData] = useState<any[]>([]);
    const [loaded, setLoaded] = useState(true);

    useEffect(() => {
        if (!loaded){
            const tempData: any[] = [];
            Promise.all(
                Array.from(Array(50).keys()).map((i) => getState((i + 1).toString()))
            ).then((responses: any[]) => {
                responses.forEach((response) => addStateDistribution(response, tempData));
                setData(tempData);
                setLoaded(true);
            });
        }
        else {
            setData(cached_data)
        }
    }, []);

    return (
        <Box>
            <Typography gutterBottom variant="subtitle1" component="div" pt={2}>
            Species Distribution by State
            </Typography>
            <Grid container justifyContent="center">
                {loaded ? (
                    <BarChart
                        width = {1000}
                        height = {400}
                        data = {data}
                        margin={{
                            top: 20,
                            right: 30,
                            left: 20,
                            bottom: 5,
                          }}
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" hide/>
                        <YAxis />
                        <Tooltip />
                        <Legend />
                        <Bar dataKey="animals" stackId="a" fill="#198387" />
                        <Bar dataKey="plants" stackId="a" fill="#198754" />
                    </BarChart>
                ) : (
                    <CircularProgress />
                )}
            </Grid>
        </Box>
    );
};


export default StateSpeciesDistributionVisualization;