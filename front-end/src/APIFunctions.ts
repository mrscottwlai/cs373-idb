import { StringDecoder } from "string_decoder";
import { createPartiallyEmittedExpression } from "typescript";

const axios = require('axios')

async function getPlant(plant_id: string) {
  try {
    console.log("About to call get")
    const request = new XMLHttpRequest();
    const link = "https://api.endangerednature.me/plants/" + plant_id;
    request.open("GET", link, false);
    request.setRequestHeader("Accept", "application/vnd.api+json");
    request.send()
    let response = null;
    if (request.status === 200){
      response = JSON.parse(request.responseText);
    }
    console.log("Response Recieved")
    console.log(response)
    return response
  } catch (error) {
    return console.log(error);
  }
}

async function getPlants(page_number: number, threatLevel: string, globalStatus: string, category: string, duration: string, plantClass: string, sortOption: string, search_query: string, nationalStatus: string) {
  try {
    //const response = await axios.get("http://api.endangerednature.me/plants");
    console.log("Making GET Request");
    console.log("page = " + page_number);
    console.log("threat = " + threatLevel);
    console.log("global = " + globalStatus);
    console.log("national = " + nationalStatus);
    console.log("category = " + category);
    console.log("duration = " + duration);
    console.log("class = " + plantClass);
    console.log("sort = " + sortOption);
    console.log("search = " + search_query);
    const request = new XMLHttpRequest();
    let link = "https://api.endangerednature.me/plants?page=" + page_number + "&perPage=25";
    if (threatLevel !== "" && threatLevel !== null) {
      link += "&threat=" + threatLevel.replace(" ", "+");
    }
    if (globalStatus !== "" && globalStatus !== null) {
      link += "&global=" + globalStatus.replace(" ", "+");
    }
    if (nationalStatus !== "" && nationalStatus !== null) {
      link += "&national=" + nationalStatus.replace(" ", "+");
    }
    if (category !== "" && category !== null) {
      link += "&category=" + category.replace(" ", "+");
    }
    if (duration !== "" && duration !== null) {
      link += "&duration=" + duration.replace(" ", "+");
    }
    if (plantClass !== "" && plantClass !== null) {
      link += "&class=" + plantClass.replace(" ", "+");
    }
    if (sortOption !== "" && sortOption !== null) {
      if (sortOption === "name-asc") {
        link += "&sort=common&ascending=true";
      } else if (sortOption === "name-desc") {
        link += "&sort=common&ascending=false";
      } else if (sortOption === "sciname-asc") {
        link += "&sort=scientific&ascending=true";
      } else if (sortOption === "sciname-desc") {
        link += "&sort=scientific&ascending=false";
      }
    }
    if (search_query !== "" && search_query !== null) {
      link += "&q=" + search_query.replace(" ", "+");
    }
    request.open("GET", link, false);
    request.setRequestHeader("Accept", "application/vnd.api+json");
    request.send()
    let response = null;
    if (request.status === 200) {
      response = JSON.parse(request.responseText);
    }
    console.log("Response Recieved")
    console.log("request = " + request.response);
    return response;
  } catch (error) {
    return console.log(error);
  }
}

async function getAnimal(animal_id: string) {
  try {
    console.log("About to call get")
    const request = new XMLHttpRequest();
    const link = "https://api.endangerednature.me/animals/" + animal_id;
    request.open("GET", link, false);
    request.setRequestHeader("Accept", "application/vnd.api+json");
    request.send()
    let response = null;
    if (request.status === 200){
      response = JSON.parse(request.responseText);
    }
    console.log("Response Recieved")
    console.log(response)
    return response
  } catch (error) {
    return console.log(error);
  }
}

async function getAnimals(page_number: number, threatLevel: string, globalStatus: string, animalClass: string, sortOption: string, search_query: string, nationalStatus: string) {
  try {
    // const response = await axios.get("api.endangerednature.me/animals");
    console.log("Making GET Request");
    console.log("page = " + page_number);
    console.log("threat = " + threatLevel);
    console.log("global = " + globalStatus);
    console.log("national = " + nationalStatus);
    console.log("class = " + animalClass);
    console.log("sort = " + sortOption);
    console.log("search = " + search_query);
    const request = new XMLHttpRequest();
    let link = "https://api.endangerednature.me/animals?page=" + page_number + "&perPage=25";
    if (threatLevel !== "" && threatLevel !== null) {
      link += "&threat=" + threatLevel.replace(" ", "+");
    }
    if (globalStatus !== "" && globalStatus !== null) {
      link += "&global=" + globalStatus.replace(" ", "+");
    }
    if (nationalStatus !== "" && nationalStatus !== null) {
      link += "&national=" + nationalStatus.replace(" ", "+");
    }
    if (animalClass !== "" && animalClass !== null) {
      link += "&class=" + animalClass.replace(" ", "+");
    }
    if (sortOption !== "" && sortOption !== null) {
      if (sortOption === "name-asc") {
        link += "&sort=common&ascending=true";
      } else if (sortOption === "name-desc") {
        link += "&sort=common&ascending=false";
      } else if (sortOption === "sciname-asc") {
        link += "&sort=scientific&ascending=true";
      } else if (sortOption === "sciname-desc") {
        link += "&sort=scientific&ascending=false";
      }
    }
    if (search_query !== "" && search_query !== null) {
      link += "&q=" + search_query.replace(" ", "+");
    }
    request.open("GET", link, false);
    request.setRequestHeader("Accept", "application/vnd.api+json");
    request.send()
    let response = null;
    if (request.status === 200) {
      response = JSON.parse(request.responseText);
    }
    console.log("Response Recieved")
    console.log("request = " + request.response)
    return response;
  } catch (error) {
    return console.log(error);
  }
}

async function getState(state_id: string) {
  try {
    console.log("About to call get")
    const request = new XMLHttpRequest();
    const link = "https://api.endangerednature.me/states/" + state_id;
    request.open("GET", link, false);
    request.setRequestHeader("Accept", "application/vnd.api+json");
    request.send()
    let response = null;
    if (request.status === 200){
      response = JSON.parse(request.responseText);
    }
    console.log("Response Recieved")
    console.log(response)
    return response
  } catch (error) {
    return console.log(error);
  }
}

async function getStates(page_number: number, sortOption: string, search_query: string, population: number[]) {
  try {
    // const response = await axios.get("api.endangerednature.me/states");
    console.log("Making GET Request");
    console.log("page = " + page_number);
    console.log("sort = " + sortOption);
    console.log("search = " + search_query);
    console.log("popMin = " + population[0]);
    console.log("popMax = " + population[1]);
    const request = new XMLHttpRequest();
    let link = "https://api.endangerednature.me/states?page=" + page_number + "&perPage=25";
    link += "&popMin=" + population[0];
    link += "&popMax=" + population[1];
    if (sortOption !== "" && sortOption !== null) {
      if (sortOption === "name-asc") {
        link += "&sort=name&ascending=true";
      } else if (sortOption === "name-desc") {
        link += "&sort=name&ascending=false";
      } else if (sortOption === "landarea-desc") {
        link += "&sort=land&ascending=false";
      } else if (sortOption === "landarea-asc") {
        link += "&sort=land&ascending=true";
      } else if (sortOption === "waterarea-desc") {
        link += "&sort=water&ascending=false";
      } else if (sortOption === "waterarea-asc") {
        link += "&sort=water&ascending=true";
      } else if (sortOption === "education-desc") {
        link += "&sort=education&ascending=false";
      } else if (sortOption === "education-asc") {
        link += "&sort=education&ascending=true";
      } else if (sortOption === "business-desc") {
        link += "&sort=businesses&ascending=false";
      } else if (sortOption === "landarea-asc") {
        link += "&sort=businesses&ascending=true";
      }
    }
    if (search_query !== "") {
      link += "&q=" + search_query.replace(" ", "+");
    }
    request.open("GET", link, false);
    request.setRequestHeader("Accept", "application/vnd.api+json");
    request.send()
    let response = null;
    if (request.status === 200) {
      response = JSON.parse(request.responseText);
    }
    console.log("Response Recieved")
    console.log("request = " + request.response);
    return response;
  } catch (error) {
    return console.log(error);
  }
}

export {
  getPlant,
  getPlants,
  getAnimal,
  getAnimals,
  getState,
  getStates
};
