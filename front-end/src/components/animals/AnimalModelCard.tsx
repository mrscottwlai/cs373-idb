import { Link } from 'react-router-dom';
import { Grid, Typography, Card, CardContent, CardMedia, CardActionArea } from '@mui/material';
import { Highlight } from 'react-highlighter-ts';
const styles = {
  card: {
    height: '100%',
    width: '95%',
    margin: 'auto',
    backgroundColor: "#b8d9c4"
  },

  media: {
    height: 200,
    width: '100%',
    objectFit: 'cover',
  },
} as const;

const AnimalModelCard = (props: any) => {
  const { animal_id, primaryCommonName, scientificName, threatLevel, population, length, weight, image, highlightSearch} =
    props;

  return (
    <Grid item key={animal_id} xs={1} sm={4} md={3}>
      <Link to={`/animals/${animal_id}`} style={{ textDecoration: 'none' }}>
        <Card variant="outlined" sx={{ maxWidth: 345 }} style={styles.card}>
          <CardActionArea style={{ outline: 'none' }}>
            <CardMedia component="img" image={image} style={styles.media} />
            <CardContent sx={{ textAlign: 'left', }}>
              <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>
                <Highlight search = {highlightSearch}>
                  {primaryCommonName}
                </Highlight>
              </Typography>
              <Typography sx={{ fontWeight: 'medium', fontStyle: 'italic', paddingBottom:2 }}>
                <Highlight search = {highlightSearch}>
                  {scientificName}
                </Highlight>
              </Typography>
              { //Check if message failed
                (threatLevel === "")
                  ? <Typography sx={{fontSize: 'caption.fontSize'}}>
                      <Highlight search = {highlightSearch}>
                        Threat Level: N/A
                      </Highlight>
                    </Typography>
                  : <Typography sx={{fontSize: 'caption.fontSize'}}>
                      <Highlight search = {highlightSearch}>
                        Threat Level: {threatLevel}
                      </Highlight>
                    </Typography>
              }
              { //Check if message failed
                (population === "")
                  ? <Typography sx={{fontSize: 'caption.fontSize'}}>
                      <Highlight search = {highlightSearch}>
                        Population: N/A
                      </Highlight>
                    </Typography>
                  : <Typography sx={{fontSize: 'caption.fontSize'}}>
                      <Highlight search = {highlightSearch}>
                        Population: {population}
                      </Highlight>
                    </Typography>
              }
              { //Check if message failed
                (length === "")
                  ? <Typography sx={{fontSize: 'caption.fontSize'}}>
                      <Highlight search = {highlightSearch}>
                        Length: N/A
                      </Highlight>
                    </Typography>
                  : <Typography sx={{fontSize: 'caption.fontSize'}}>
                      <Highlight search = {highlightSearch}>
                        Length: {length} cm
                      </Highlight>
                    </Typography>
              }
              { //Check if message failed
                (weight === "")
                  ? <Typography sx={{fontSize: 'caption.fontSize'}}>
                      <Highlight search = {highlightSearch}>
                        Weight: N/A
                      </Highlight>
                    </Typography>
                  : <Typography sx={{fontSize: 'caption.fontSize'}}>
                      <Highlight search = {highlightSearch}>
                        Weight: {weight} g
                      </Highlight>
                    </Typography>
              }
            </CardContent>
          </CardActionArea>
        </Card>
      </Link>
    </Grid>
  );
};

export default AnimalModelCard;
