import os
from venv import create
from flask import Flask, render_template, request, url_for, redirect, json
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from sqlalchemy import inspect
from database import create_db

# from cache import create_cache
import restapi

app = restapi.create_app()
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql://postgres:WduYzS*XB$9V3^@database.cvxhhajhfxy5.us-east-2.rds.amazonaws.com:5432/myDatabase"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
create_db(app)
# create_cache(app)
CORS(app)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
