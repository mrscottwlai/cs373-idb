import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

import chromedriver_autoinstaller

# Maybe
import re

URL = "https://dev.d30vxib9rhm42v.amplifyapp.com"

class TestFrontendGui(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("window-size=1200x600")
        chrome_options.add_argument("--log-level=3")
        chrome_options.add_argument("--disable-gpu")
        chrome_prefs = {}
        chrome_options.experimental_options["prefs"] = chrome_prefs
        # Disable images
        chrome_prefs["profile.default_content_settings"] = {"images":2}

        self.driver = webdriver.Chrome(options = chrome_options)
        self.url = URL

    def tearDown(self):
        self.driver.quit()

    
    # Check navbar buttons exist and are ordered
    def testNavBarListing(self):
        self.driver.get(self.url)

        expected_navbar = ['about','animals','plants','states']
        buttons = self.driver.find_elements(by = By.CLASS_NAME, value = "MuiButtonBase-root")

        recieved_navbar = buttons[1:5]
        for expected, recieved in zip(expected_navbar,recieved_navbar):
            self.assertEqual(expected, recieved.text.lower())


    # Check Navbar buttons lead to correct location
    def testAboutNavBarButton(self):
        self.driver.get(self.url)

        about_button = self.driver.find_elements(by = By.CLASS_NAME, value = "MuiButtonBase-root")[1]
        about_button.click()
        self.assertEqual(self.driver.current_url, self.url + "/about")

    def testAnimalstNavBarButton(self):
        self.driver.get(self.url)

        animals_button = self.driver.find_elements(by = By.CLASS_NAME, value = "MuiButtonBase-root")[2]
        animals_button.click()
        self.assertEqual(self.driver.current_url, self.url + "/animals")

    def testPlantsNavBarButton(self):
        self.driver.get(self.url)

        plants_button = self.driver.find_elements(by = By.CLASS_NAME, value = "MuiButtonBase-root")[3]
        plants_button.click()
        self.assertEqual(self.driver.current_url, self.url + "/plants")

    def testStatesNavBarButton(self):
        self.driver.get(self.url)

        states_button = self.driver.find_elements(by = By.CLASS_NAME, value = "MuiButtonBase-root")[4]
        states_button.click()
        self.assertEqual(self.driver.current_url, self.url + "/states")

    # Check Animal Instances exist and are clickable
    def testAnimalsExist(self):
        self.driver.get(self.url + "/animals")

        animals = self.driver.find_elements(by = By.CLASS_NAME, value = "MuiGrid-item")
        self.assertGreater(len(animals), 0)

    def testAnimalClickable(self):
        self.driver.get(self.url + "/animals")

        animals = self.driver.find_elements(by = By.CLASS_NAME, value = "MuiGrid-item")
        animals[2].click()
        self.assertEqual(self.driver.current_url, self.url + "/animals/154")

    # Check Plant Instances exist
    def testPlantsExist(self):
        self.driver.get(self.url + "/plants")
        
        plants = self.driver.find_elements(by = By.CLASS_NAME, value = "MuiGrid-item")
        self.assertGreater(len(plants), 0)

    def testPlantsClickable(self):
        self.driver.get(self.url + "/plants")

        plants = self.driver.find_elements(by = By.CLASS_NAME, value = "MuiGrid-item")
        plants[2].click()
        self.assertEqual(self.driver.current_url, self.url + "/plants/45")

    # Check States Instances exist
    def testStatesExist(self):
        self.driver.get(self.url + "/states")
        
        states = self.driver.find_elements(by = By.CLASS_NAME, value = "MuiGrid-item")
        self.assertGreater(len(states), 0)

    def testStatesClickable(self):
        self.driver.get(self.url + "/states")

        states = self.driver.find_elements(by = By.CLASS_NAME, value = "MuiGrid-item")
        states[2].click()
        self.assertEqual(self.driver.current_url, self.url + "/states/2")
    
    # Filtering Tests
    def testFilter(self):
        self.driver.get(self.url + "/animals")

        self.driver.find_elements(by = By.CLASS_NAME, value = "MuiFormControl-root")[1].click()
        self.driver.find_elements(by = By.CLASS_NAME, value = "MuiMenuItem-root")[6].click()
        self.driver.find_elements(by = By.CLASS_NAME, value = "MuiGrid-item")[2].click()

        self.assertEqual(self.driver.current_url, self.url + "/animals/40")


    # Searching Tests
    def testSearching(self):
        self.driver.get(self.url + "/plants")

        self.driver.find_elements(by = By.CLASS_NAME, value = "MuiInputBase-input")[1].send_keys("Blolly",Keys.ENTER)
        self.driver.find_elements(by = By.CLASS_NAME, value = "MuiGrid-item")[2].click()

        self.assertEqual(self.driver.current_url, self.url + "/plants/84")

    # Sorting Test
    def testSorting(self):
        self.driver.get(self.url + "/states")

        self.driver.find_elements(by = By.CLASS_NAME, value = "MuiFormControl-root")[1].click()
        self.driver.find_elements(by = By.CLASS_NAME, value = "MuiMenuItem-root")[10].click()
        self.driver.find_elements(by = By.CLASS_NAME, value = "MuiGrid-item")[2].click()

        self.assertEqual(self.driver.current_url, self.url + "/states/19")

if __name__ == "__main__":
    chromedriver_autoinstaller.install()
    unittest.main()