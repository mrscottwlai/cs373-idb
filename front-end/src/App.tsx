import React from 'react';
import { Routes, Route, Outlet} from "react-router-dom";


import Navigation from "./components/Navbar"
import Home from "./components/Home"
import About from './components/About';
import Animals from "./components/animals/Animals"
import Animal from "./components/animals/AnimalInstanceCard"
import Plants from "./components/plants/Plants"
import Plant from "./components/plants/PlantInstanceCard"
import States from "./components/states/States"
import State from "./components/states/StateInstanceCard"
import Search from "./components/SearchPage"
import Visualizations from './VisualizationsPage';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
      <Navigation />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/about" element={<About />} />
        <Route path="/animals" element={<Animals />} />
        <Route path="/animals/:id" element={<Animal />} />
        <Route path="/plants" element={<Plants />} />
        <Route path="/plants/:id" element={<Plant />} />
        <Route path="/states" element={<States />} />
        <Route path="/states/:id" element={<State />} />
        <Route path="/search/:id" element={<Search />} />
        <Route path="/visualizations" element={<Visualizations />} />
        <Route path="*" element={
          <main>
            Page does not exist!
          </main>
        } />
      </Routes>
      <Outlet />
    </div>
  );
}

export default App;
