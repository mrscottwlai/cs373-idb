import { CardActionArea, Box, List, ListItem, ListItemText, Typography, Card, CardContent, CardMedia, CssBaseline, Grid, Container } from '@mui/material';
import StateInstanceGrid from "./StateInstanceGrid"

function States() {
    return (
        <>
            <CssBaseline />
            <main>
                <Container>
                    <Typography variant="h1" align="center" gutterBottom>
                        States
                    </Typography>
                </Container>
                <hr/>
                <StateInstanceGrid />
            </main>
        </>
    );
}

export default States;
