from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def create_db(app):
    global db
    db = SQLAlchemy(app, session_options={"autoflush": False})


class State(db.Model):
    __tablename__ = "state_table"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True, nullable=False)
    nickname = db.Column(db.String(50))
    abbreviation = db.Column(db.String(2), unique=True, nullable=False)
    population = db.Column(db.Integer)
    capital = db.Column(db.String(50))
    majorCities = db.relationship("City")
    landArea = db.Column(db.Numeric)
    waterArea = db.Column(db.Numeric)
    businessNumber = db.Column(db.Numeric)
    educationalAttainment = db.Column(db.Numeric)
    borderingStates = db.relationship(
        "State",
        secondary="border_table",
        back_populates="borderingStates",
        primaryjoin="State.id==Border.left_id",
        secondaryjoin="State.id==Border.right_id",
    )
    majorIndustries = db.relationship("Industry")
    plants = db.relationship("PlantState", back_populates="state")
    animals = db.relationship("AnimalState", back_populates="state")
    image = db.Column(db.String(300))
    map = db.Column(db.String(300))
    flag = db.Column(db.String(300))
    name_dict = {
        "name": "name",
        "land": "landArea",
        "water": "waterArea",
        "businesses": "businessNumber",
        "education": "educationalAttainment",
        "nick": "nickname",
        "abbr": "abbreviation",
        "pop": "population",
        "capital": "capital",
    }


class Border(db.Model):
    __tablename__ = "border_table"
    left_id = db.Column(db.Integer, db.ForeignKey("state_table.id"), primary_key=True)
    right_id = db.Column(db.Integer, db.ForeignKey("state_table.id"), primary_key=True)


class City(db.Model):
    name = db.Column(db.String(50), primary_key=True)
    state = db.Column(db.Integer, db.ForeignKey("state_table.id"), primary_key=True)


class Industry(db.Model):
    name = db.Column(db.String(100), primary_key=True)
    state = db.Column(db.Integer, db.ForeignKey("state_table.id"), primary_key=True)


class Animal(db.Model):
    __tablename__ = "animal_table"
    id = db.Column(db.Integer, primary_key=True)
    scientificName = db.Column(db.String(50), unique=True)
    primaryCommonName = db.Column(db.String(50))
    taxonomyKingdom = db.Column(db.String(50))
    taxonomyPhylum = db.Column(db.String(50))
    taxonomyClass = db.Column(db.String(50))
    taxonomyOrder = db.Column(db.String(50))
    taxonomyFamily = db.Column(db.String(50))
    taxonomyGenus = db.Column(db.String(50))
    threatLevel = db.Column(db.String(50))
    states = db.relationship("AnimalState", back_populates="animal")
    population = db.Column(db.String(50))
    globalStatus = db.Column(db.String(50))
    nationalStatus = db.Column(db.String(50))
    populationTrend = db.Column(db.String(50))
    foodHabits = db.relationship("FoodHabit")
    length = db.Column(db.Numeric)
    weight = db.Column(db.Numeric)
    nsxUrl = db.Column(db.String(100))
    habitats = db.relationship(
        "Habitat", secondary="animal_habitat_join", back_populates="animals"
    )
    nameCategory = db.Column(db.String(50))
    image = db.Column(db.String(500))
    video = db.Column(db.String(500))
    name_dict = {
        "class": "taxonomyClass",
        "length": "length",
        "weight": "weight",
        "global": "globalStatus",
        "national": "nationalStatus",
        "threat": "threatLevel",
        "scientific": "scientificName",
        "common": "primaryCommonName",
        "nameCat": "nameCategory",
    }


class FoodHabit(db.Model):
    habit = db.Column(db.String(50), primary_key=True)
    animal = db.Column(db.Integer, db.ForeignKey("animal_table.id"), primary_key=True)
    adultHabit = db.Column(db.Boolean)
    immatureHabit = db.Column(db.Boolean)


class Habitat(db.Model):
    __tablename__ = "habitat_table"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30))
    animals = db.relationship(
        "Animal", secondary="animal_habitat_join", back_populates="habitats"
    )
    plants = db.relationship(
        "Plant", secondary="plant_habitat_join", back_populates="habitats"
    )


class AnimalHabitat(db.Model):
    __tablename__ = "animal_habitat_join"
    animal_id = db.Column(
        db.Integer, db.ForeignKey("animal_table.id"), primary_key=True
    )
    habitat_id = db.Column(
        db.Integer, db.ForeignKey("habitat_table.id"), primary_key=True
    )


class PlantHabitat(db.Model):
    __tablename__ = "plant_habitat_join"
    plant_id = db.Column(db.Integer, db.ForeignKey("plant_table.id"), primary_key=True)
    habitat_id = db.Column(
        db.Integer, db.ForeignKey("habitat_table.id"), primary_key=True
    )


class Plant(db.Model):
    __tablename__ = "plant_table"
    id = db.Column(db.Integer, primary_key=True)
    scientificName = db.Column(db.String(50), unique=True)
    primaryCommonName = db.Column(db.String(50))
    taxonomyKingdom = db.Column(db.String(50))
    taxonomyPhylum = db.Column(db.String(50))
    taxonomyClass = db.Column(db.String(50))
    taxonomyOrder = db.Column(db.String(50))
    taxonomyFamily = db.Column(db.String(50))
    taxonomyGenus = db.Column(db.String(50))
    threatLevel = db.Column(db.String(50))
    states = db.relationship("PlantState", back_populates="plant")
    category = db.Column(db.String(50))
    economicUses = db.relationship("EconomicUse")
    duration = db.Column(db.String(50))
    growthHabit = db.Column(db.String(50))
    globalStatus = db.Column(db.String(50))
    nationalStatus = db.Column(db.String(50))
    nsxUrl = db.Column(db.String(100))
    habitats = db.relationship(
        "Habitat", secondary="plant_habitat_join", back_populates="plants"
    )
    nameCategory = db.Column(db.String(50))
    image = db.Column(db.String(500))
    video = db.Column(db.String(500))
    name_dict = {
        "class": "taxonomyClass",
        "category": "category",
        "duration": "duration",
        "habit": "growthHabit",
        "global": "globalStatus",
        "national": "nationalStatus",
        "threatLevel": "threatLevel",
        "scientific": "scientificName",
        "common": "primaryCommonName",
        "nameCat": "nameCategory",
    }


class EconomicUse(db.Model):
    use = db.Column(db.String(50), primary_key=True)
    plant = db.Column(db.Integer, db.ForeignKey("plant_table.id"), primary_key=True)


class AnimalState(db.Model):
    __tablename__ = "animal_state_join"
    animal_id = db.Column(
        db.Integer, db.ForeignKey("animal_table.id"), primary_key=True
    )
    state_id = db.Column(db.Integer, db.ForeignKey("state_table.id"), primary_key=True)
    animal = db.relationship("Animal", back_populates="states")
    state = db.relationship("State", back_populates="animals")
    hybrid = db.Column(db.Boolean)
    exotic = db.Column(db.Boolean)
    native = db.Column(db.Boolean)
    status = db.Column(db.String(30))


class PlantState(db.Model):
    __tablename__ = "plant_state_join"
    plant_id = db.Column(db.Integer, db.ForeignKey("plant_table.id"), primary_key=True)
    state_id = db.Column(db.Integer, db.ForeignKey("state_table.id"), primary_key=True)
    plant = db.relationship("Plant", back_populates="states")
    state = db.relationship("State", back_populates="plants")
    hybrid = db.Column(db.Boolean)
    exotic = db.Column(db.Boolean)
    native = db.Column(db.Boolean)
    status = db.Column(db.String(30))


# Code to make a database
# db.drop_all()
# db.create_all()

# states_json = open('states.json')
# states = json.load(states_json)
# for state in states:
#     record = State()
#     record.name = state['name']
#     record.nickname = state['nickname']
#     record.abbreviation = state['abbreviation']
#     record.population = state['population']
#     record.capital = state['capital']
#     for city in state['majorCities']:
#         new_city = City(name=city)
#         record.majorCities.append(new_city)
#         db.session.add(new_city)
#     record.landArea = state['landArea']
#     record.waterArea = state['waterArea']
#     record.businessNumber = state['businessNumber']
#     record.educationalAttainment = state['educationalAttainment']
#     for borderingState in state['borderingStates']:
#         name = borderingState['name']
#         if db.session.query(State).filter_by(name=name).first() is not None:
#             other_state = db.session.query(State).filter_by(name=name).first()
#             record.borderingStates.append(other_state)
#             other_state.borderingStates.append(record)
#     for industry in state['majorIndustries']:
#         new_industry = Industry(name=industry)
#         record.majorIndustries.append(new_industry)
#         db.session.add(new_industry)
#     record.image = state['image']
#     record.map = state['map']
#     record.flag = state['flag']
#     db.session.add(record)

# db.session.commit()

# animals_json = open('animals.json')
# animals = json.load(animals_json)
# for animal in animals:
#     record = Animal()
#     record.scientificName = animal['scientificName']
#     record.primaryCommonName = animal['primaryCommonName']
#     record.taxonomyKingdom = animal['kingdom']
#     record.taxonomyPhylum = animal['phylum']
#     record.taxonomyClass = animal['class']
#     record.taxonomyOrder = animal['order']
#     record.taxonomyFamily = animal['family']
#     record.taxonomyGenus = animal['genus']
#     record.threatLevel = animal['threatLevel']
#     for s in animal['states']:
#         state = db.session.query(State).filter_by(name=s['name']).first()
#         assoc = AnimalState()
#         assoc.hybrid = s['hybrid']
#         assoc.exotic = s['exotic']
#         assoc.native = s['native']
#         assoc.status = s['status']
#         db.session.add(assoc)
#         record.states.append(assoc)
#         state.animals.append(assoc)
#     record.population = animal['population']
#     record.globalStatus = animal['globalStatus']
#     record.nationalStatus = animal['nationalStatus']
#     record.populationTrend = animal['populationTrend']
#     for habit in animal['foodHabits']:
#         new_foodHabit = FoodHabit(habit=habit['foodHabit'])
#         new_foodHabit.adultHabit = habit['adultHabit']
#         new_foodHabit.immatureHabit = habit['immatureHabit']
#         record.foodHabits.append(new_foodHabit)
#         db.session.add(new_foodHabit)
#     record.length = animal['length']
#     record.weight = animal['weight']
#     record.nsxUrl = animal['nsxUrl']
#     for habitat in animal['habitats']:
#         if db.session.query(Habitat).filter_by(name=habitat).first() is not None:
#             new_habitat = db.session.query(Habitat).filter_by(name=habitat).first()
#         else:
#             new_habitat = Habitat(name=habitat)
#         record.habitats.append(new_habitat)
#         # new_habitat.animals.append(record)
#         db.session.add(new_habitat)
#     record.nameCategory = animal['nameCategory']
#     record.image = animal['image']
#     record.video = animal['video']
#     db.session.add(record)

# db.session.commit()

# plants_json = open('plants.json')
# plants = json.load(plants_json)
# for plant in plants:
#     record = Plant()
#     record.scientificName = plant['scientificName']
#     record.primaryCommonName = plant['primaryCommonName']
#     record.taxonomyKingdom = plant['kingdom']
#     record.taxonomyPhylum = plant['phylum']
#     record.taxonomyClass = plant['class']
#     record.taxonomyOrder = plant['order']
#     record.taxonomyFamily = plant['family']
#     record.taxonomyGenus = plant['genus']
#     record.threatLevel = plant['threatLevel']
#     for s in plant['states']:
#         state = db.session.query(State).filter_by(name=s['name']).first()
#         assoc = PlantState()
#         assoc.hybrid = s['hybrid']
#         assoc.exotic = s['exotic']
#         assoc.native = s['native']
#         assoc.status = s['status']
#         db.session.add(assoc)
#         record.states.append(assoc)
#         state.plants.append(assoc)
#     record.category = plant['category']
#     for use in plant['economicUses']:
#         new_use = EconomicUse(use=use)
#         db.session.add(new_use)
#         record.economicUses.append(new_use)
#     record.duration = plant['duration']
#     record.growthHabit = plant['growthHabit']
#     record.globalStatus = plant['globalStatus']
#     record.nationalStatus = plant['nationalStatus']
#     record.nsxUrl = plant['nsxUrl']
#     for habitat in plant['habitats']:
#         if db.session.query(Habitat).filter_by(name=habitat).first() is not None:
#             new_habitat = db.session.query(Habitat).filter_by(name=habitat).first()
#         else:
#             new_habitat = Habitat(name=habitat)
#         record.habitats.append(new_habitat)
#         # new_habitat.plants.append(record)
#         db.session.add(new_habitat)
#     record.nameCategory = animal['nameCategory']
#     record.image = animal['image']
#     record.video = animal['video']
#     db.session.add(record)

# db.session.commit()


# @app.route("/")
# def hello_world():
#     return '<img src="https://i.kym-cdn.com/photos/images/original/001/211/814/a1c.jpg" alt="cowboy" />'
