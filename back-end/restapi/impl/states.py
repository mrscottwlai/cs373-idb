import json
import time
from database import (
    db,
    State,
    Border,
    City,
    Industry,
    Animal,
    FoodHabit,
    Habitat,
    AnimalHabitat,
    PlantHabitat,
    Plant,
    EconomicUse,
    AnimalState,
    PlantState,
)
from flask import request
from sqlalchemy.orm import selectinload
import common


def StateToDict(state, details):
    dictionary = common.object_as_dict(state)
    dictionary["majorCities"] = [city.name for city in state.majorCities]
    dictionary["majorIndustries"] = [
        industry.name for industry in state.majorIndustries
    ]
    dictionary["borderingStates"] = [
        borderingState.name for borderingState in state.borderingStates
    ]
    if details:
        dictionary["plants"] = [
            {"id": plantState.plant_id, "name": plantState.plant.primaryCommonName}
            for plantState in state.plants
        ]
        dictionary["animals"] = [
            {"id": animalState.animal_id, "name": animalState.animal.primaryCommonName}
            for animalState in state.animals
        ]
    return dictionary


def GetStates():
    startTime = time.time()
    # attributes adjusted by a slider
    range_filter_fields = [
        State.population,
        State.landArea,
        State.waterArea,
        State.businessNumber,
        State.educationalAttainment,
    ]
    # attributes to sort by
    sort_fields = [
        State.name,
        State.nickname,
        State.abbreviation,
        State.population,
        State.capital,
        State.landArea,
        State.waterArea,
        State.businessNumber,
        State.educationalAttainment,
    ]
    search_fields = [State.name, State.nickname, State.abbreviation, State.capital]
    # get dict of queries
    queries = request.args.to_dict(flat=False)
    # create BaseQuery
    states = db.session.query(State).options(
        selectinload(State.majorCities),
        selectinload(State.majorIndustries),
        selectinload(State.borderingStates),
    )
    # get page data from query
    page = common.get_query("page", queries)
    page = 1 if page == None else int(page[0])
    per_page = (
        int(common.get_query("perPage", queries).pop())
        if common.get_query("perPage", queries)
        else 20
    )
    # get filter data from query
    try:
        query_field = common.parse_request_param(request.args, "q", str)
        range_filters = common.parse_range_filters(
            request.args, State, range_filter_fields
        )
        sort_logic = common.parse_sort_fields(
            request.args, State, sort_fields, State.name
        )
    except Exception as e:
        return {"status": "fail", "data": e.args}
    # apply search data to BaseQuery
    if query_field is not None:
        states = common.add_search_filters(states, search_fields, query_field)
    # apply filter data to BaseQuery
    if len(range_filters) > 0:
        states = common.add_range_filters(states, range_filters)
    # apply sorting to BaseQuery
    if sort_logic is None:
        states = states.order_by(State.primaryCommonName.desc())
    else:
        states = states.order_by(sort_logic)
    # paginate using given page queries
    state_page = states.paginate(page=page, per_page=per_page)
    # get instances from just one of the pages
    result = state_page.items
    # format to JSON
    common.printTimeDebug("/states before ToDict", startTime)
    outputList = [StateToDict(row, False) for row in result]
    # combine # of instances in query & a query page into a dict
    ret = {"count": states.count(), "page": outputList}
    # print(ret)
    common.printTimeDebug("/states", startTime)
    return json.dumps(ret, cls=common.DecimalEncoder, indent=4), 200


def GetStateId(options):
    """
    :param options: A dictionary containing all the paramters for the Operations
        options["stateId"]: The unique identifier of the state
    """
    startTime = time.time()
    state = (
        db.session.query(State)
        .options(
            selectinload(State.animals).selectinload(AnimalState.animal),
            selectinload(State.plants).selectinload(PlantState.plant),
            selectinload(State.majorCities),
            selectinload(State.majorIndustries),
            selectinload(State.borderingStates),
        )
        .get(options["stateId"])
    )
    outputDict = StateToDict(state, True)
    common.printTimeDebug("/states/id", startTime)
    return json.dumps(outputDict, cls=common.DecimalEncoder, indent=4), 200
