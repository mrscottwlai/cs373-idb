import { Box, Typography } from '@mui/material';
import ExampleVisualization from './visualizations/ExampleVisualization';
import ProviderJobDistributionVisualization from './visualizations/ProviderJobDistributionVisualization';
import ProviderCityBudgetSafetyScoreVisualization from './visualizations/ProviderCityBudgetSafetyScoreVisualization';
import ProviderApartmentTypeDistributionVisualization from './visualizations/ProviderApartmentTypeDistributionVisualization';

const ProviderVisualizations = () => {
  return (
    <Box>
      <Typography gutterBottom variant="h5" component="div" pt={2}>
        GeoJobs Provider Visualizations
      </Typography>
      <ProviderJobDistributionVisualization/>
      <ProviderApartmentTypeDistributionVisualization/>
      <ProviderCityBudgetSafetyScoreVisualization/>
    </Box>
  );
};

export default ProviderVisualizations;
