import { Link, useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
//import { Highlight } from 'react-highlighter-ts';
import { getHighlightSearch } from './HighlightSearch';
import {
  Box,
  Typography,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  CircularProgress,
} from '@mui/material';
import AnimalModelCard from './animals/AnimalModelCard';
import PlantModelCard from './plants/PlantModelCard';
import StateModelCard from './states/StateModelCard';
import { getAnimals, getPlants, getStates } from '../APIFunctions';

const Search = () => {
    const query = useParams().id || '';
    const emptyVal: any = null;

    const [animalData, setAnimalsData] = useState<any[]>([]);
    const [plantData, setPlantData] = useState<any[]>([]);
    const [stateData, setStateData] = useState<any[]>([]);

    const [loaded, setLoaded] = useState(false);
    const [page, setPage] = useState(0);

    useEffect(() => {
        getAnimals(page+1, emptyVal, emptyVal, emptyVal, emptyVal, query, emptyVal).then(
            (response: any) => {
                setAnimalsData(response['page']);
                getPlants(page+1, emptyVal, emptyVal, emptyVal, emptyVal, emptyVal, emptyVal, query, emptyVal).then(
                    (response: any) => {
                        setPlantData(response['page']);
                        getStates(page+1, emptyVal, query, [0,40000000]).then(
                            (response: any) => {
                                setStateData(response['page']);
                                setLoaded(true);
                            }
                        )
                    }
                )
            }
        );
    }, [query]);

    console.log("Search Query: " + query);

    return (
        <Box>
            <Typography id="animalSectionTitle" gutterBottom variant="h3" component="div" pt={2}>
                Animals
            </Typography>
            
            {loaded ? (
                <Grid
                    container
                    spacing = {{ xs: 2, md: 3 }}
                    columns = {{ xs: 2, sm: 8, md: 16 }}
                    p={3}
                    justifyContent="center"
                >

                {
                    animalData.map((animal, i) => (
                        <AnimalModelCard
                            key={animal.id}
                            animal_id={animal.id}
                            primaryCommonName={animal.primaryCommonName}
                            scientificName={animal.scientificName}
                            threatLevel={animal.threatLevel}
                            population={animal.population}
                            image={animal.image}
                            highlightSearch={getHighlightSearch(query)}
                        />
                    ))
                }

                </Grid>
            ) : (
                <CircularProgress />
            )}

            <Typography id="plantSectionTitle" gutterBottom variant="h3" component="div" pt={2}>
                Plants
            </Typography>
            
            {loaded ? (
                <Grid
                    container
                    spacing = {{ xs: 2, md: 3 }}
                    columns = {{ xs: 2, sm: 8, md: 16 }}
                    p={3}
                    justifyContent="center"
                >

                {
                    plantData.map((plant, i) => (
                        <PlantModelCard
                            key={plant.id}
                            plant_id={plant.id}
                            primaryCommonName={plant.primaryCommonName}
                            scientificName={plant.scientificName}
                            threatLevel={plant.threatLevel}
                            category={plant.category}
                            duration={plant.duration}
                            growthHabit={plant.growthHabit}
                            image={plant.image}
                            highlightSearch={getHighlightSearch(query)}
                        />
                    ))
                }

                </Grid>
            ) : (
                <CircularProgress />
            )}

            <Typography id="stateSectionTitle" gutterBottom variant="h3" component="div" pt={2}>
                States
            </Typography>
            
            {loaded ? (
                <Grid
                    container
                    spacing = {{ xs: 2, md: 3 }}
                    columns = {{ xs: 2, sm: 8, md: 16 }}
                    p={3}
                    justifyContent="center"
                >

                {
                    stateData.map((state, i) => (
                        <StateModelCard
                            key={state.id}
                            state_id={state.id}
                            name={state.name}
                            pop={state.population}
                            abbreviation={state.abbreviation}
                            educationalAttainment={state.educationalAttainment}
                            businessNumber={state.businessNumber}
                            landArea={state.landArea}
                            waterArea={state.waterArea}
                            image={state.image}
                            highlightSearch={getHighlightSearch(query)}
                        />
                    ))
                }

                </Grid>
            ) : (
                <CircularProgress />
            )}

        </Box>
    )
}

export default Search;