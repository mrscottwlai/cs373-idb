import json
import time
from decimal import *
from sqlalchemy import and_, or_, func, inspect

timeDebug = True


def printTimeDebug(endpoint, startTime):
    if timeDebug:
        print(endpoint + ": " + str(time.time() - startTime))


def object_as_dict(obj):
    return {c.key: getattr(obj, c.key) for c in inspect(obj).mapper.column_attrs}


class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            return str(obj)
        return json.JSONEncoder.default(self, obj)


def parse_request_param(req_args, name, type, list=False):
    if name in req_args:
        try:
            if list:
                args = req_args[name].split(",")
            else:
                args = [req_args[name]]
            ret = []
            for arg in args:
                ret.append(type(arg))
            if list:
                return ret
            else:
                return ret[0]
        except:
            raise Exception({name: name + " is invalid"})
    else:
        if list:
            return []
        else:
            return None


def parse_exact_filters(req_args, model, exact_filter_fields):
    filters = {}
    inv_name_dict = {v: k for k, v in model.name_dict.items()}
    for field in exact_filter_fields:
        # print(type(field))
        field_name = inv_name_dict[field.key]
        field_type = field.type.python_type
        # print(field_name)
        if field_type is list:
            field_type = str
        result = parse_request_param(req_args, field_name, field_type, list=True)
        # print(result)
        if len(result) > 0:
            filters[field] = result
    return filters


def add_exact_filters(base_query, filters):
    query_filters = []
    for field in filters:
        if field.type.python_type is list:
            query_filters.append(field.contains(filters[field]))
        else:
            query_filters.append(field.in_(filters[field]))
    return base_query.filter(*query_filters)


def parse_sort_fields(req_args, model, sort_fields, default_field):
    sort_field_n = parse_request_param(req_args, "sort", str)
    do_ascending = parse_request_param(req_args, "ascending", str)
    if do_ascending is not None:
        do_ascending = do_ascending.lower() == "true"
    else:
        do_ascending = True
    chosen_field = default_field
    if sort_field_n is not None:
        inv_name_dict = {v: k for k, v in model.name_dict.items()}
        for field in sort_fields:
            field_name = inv_name_dict[field.name]
            if field_name == sort_field_n:
                chosen_field = field
                break
    if do_ascending:
        return chosen_field.asc()
    else:
        return chosen_field.desc()
    return None


def add_range_filters(base_query, filters):
    query_filters = []
    for field in filters:
        min, max = filters[field]
        if min is not None:
            query_filters.append(field >= min)
        if max is not None:
            query_filters.append(field <= max)
    return base_query.filter(and_(*query_filters))


def parse_range_filters(req_args, model, range_filter_fields):
    filters = {}
    inv_name_dict = {v: k for k, v in model.name_dict.items()}
    for field in range_filter_fields:
        field_name = inv_name_dict[field.name]
        min = parse_request_param(req_args, field_name + "Min", int)
        max = parse_request_param(req_args, field_name + "Max", int)
        if min is not None or max is not None:
            filters[field] = (min, max)
    return filters


def get_query(name, queries):
    try:
        return queries[name]
    except KeyError:
        return None


def add_search_filters(base_query, fields, val):
    search_str = "%"
    for s in val.split():
        search_str += s + "%"
    search_args = []
    for field in fields:
        if field.type.python_type is list:
            search_args.append(func.array_to_string(field, ",").ilike(search_str))
        else:
            search_args.append(field.ilike(search_str))
    return base_query.filter(or_(*search_args))
