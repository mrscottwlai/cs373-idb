from flask import Blueprint
from .. import impl

bp = Blueprint("plants", __name__)


@bp.route("/plants", methods=["get"])
def GetPlants():

    return impl.plants.GetPlants()


@bp.route("/plants/<plantId>", methods=["get"])
def GetPlantId(plantId):

    options = {}
    options["plantId"] = plantId

    return impl.plants.GetPlantId(options)
