from flask import Blueprint
from .. import impl

bp = Blueprint("animals", __name__)


@bp.route("/animals", methods=["get"])
def GetAnimals():

    return impl.animals.GetAnimals()


@bp.route("/animals/<animalId>", methods=["get"])
def GetAnimalId(animalId):

    options = {}
    options["animalId"] = animalId

    return impl.animals.GetAnimalId(options)
