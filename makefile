.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

FRONTEND_DIR = front-end
BACKEND_DIR = back-end

install:
	pip install -r ./$(BACKEND_DIR)/requirements.txt

format:
	black ./$(BACKEND_DIR)/*.py

all:

# Installs node packages
frontend-install:
	cd $(FRONTEND_DIR) \
	npm install

# Runs the development server on your local machine
frontend-start:
	cd $(FRONTEND_DIR) \
	npm start

run:
	$(MAKE) install
	$(MAKE) start

# check files, check their existence with make check
CFILES :=                                 \
    .gitignore                            \
    .gitlab-ci.yml 

# check the existence of check files
check: $(CFILES)
