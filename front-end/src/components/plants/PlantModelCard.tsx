import { Link } from 'react-router-dom';
import { Grid, Typography, Card, CardContent, CardMedia, CardActionArea } from '@mui/material';
import { Highlight } from 'react-highlighter-ts';
const styles = {
  card: {
    height: '100%',
    width: '95%',
    margin: 'auto',
  },

  media: {
    height: 280,
    width: '100%',
    objectFit: 'cover',
  },
} as const;

const PlantModelCard = (props: any) => {
  const { plant_id, primaryCommonName, scientificName, threatLevel, category, duration, growthHabit, image, highlightSearch} =
    props;

  return (
    <Grid item key={plant_id} xs={1} sm={4} md={3}>
      <Link to={`/plants/${plant_id}`} style={{ textDecoration: 'none' }}>
        <Card variant="outlined" sx={{ maxWidth: 345 }} style={styles.card}>
          <CardActionArea style={{ outline: 'none' }}>
            <CardMedia component="img" image={image} style={styles.media} />
            <CardContent sx={{ textAlign: 'left', }}>
              <Typography sx={{ fontWeight: 'bold', fontSize: 'h7.fontSize'}}>
                <Highlight search = {highlightSearch}>
                  {primaryCommonName}
                </Highlight>
              </Typography>
              <Typography sx={{ fontWeight: 'medium', fontStyle: 'italic', paddingBottom:2 }}>
                <Highlight search = {highlightSearch}>
                  {scientificName}
                </Highlight>
              </Typography>
              {
                (threatLevel === null || threatLevel === "")
                  ? <Typography sx={{fontSize: 'caption.fontSize'}}>
                      <Highlight search = {highlightSearch}>
                        Threat Level: N/A
                      </Highlight>
                    </Typography>
                  : <Typography sx={{fontSize: 'caption.fontSize'}}>
                      <Highlight search = {highlightSearch}>
                        Threat Level: {threatLevel}
                      </Highlight>
                    </Typography>
              }
              <Typography sx={{fontSize: 'caption.fontSize'}}>
                <Highlight search = {highlightSearch}>
                  Category: {category}
                </Highlight>
              </Typography>
              <Typography sx={{fontSize: 'caption.fontSize'}}>
                <Highlight search = {highlightSearch}>
                  Duration: {duration}
                </Highlight>
              </Typography>
              <Typography sx={{fontSize: 'caption.fontSize'}}>
                <Highlight search = {highlightSearch}>
                  Growth Habit: {growthHabit}
                </Highlight>
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      </Link>
    </Grid>
  );
};

export default PlantModelCard;
