# C S 373 IDB: EndangeredNature

## Group Number
10-6

## Members
| Name          | UTEID   | GitLab ID      |
| ------------- | ------- | -------------- |
| Scott Lai     | swl593  | @mrscottwlai   |
| Colin Liu     | ckl658  | @speedycatfish |
| Victor Favela | vdf244  | @VictorFavela  |
| Brian Wan     | btw659  | @wanbrian      |
| Lucas Zimmer  | lz6983  | @lcz0103       |


## Project Leader and Git SHA

| Phase # | Leader        | Git SHA                                  |
| ------- | ------------- | ---------------------------------------- |
| 1       | Lucas Zimmer  | 61638bdbda30fa990447a9bd8ad9def5d332b4a4 |
| 2       | Colin Liu     | bd3fe450518240a2cdee470e0c6ca1a32df61674 |
| 3       | Scott Lai     | 6f124799ae138d0e4604b500c770534eefaf3be7 |
| 4       | Victor Favela | e868b5b54f304a3130cd7c776043ef9860cd2692 |

Leader's Responisibility: The leader is well-informed on all aspects of the project phase, organizes the team to perform effectively, and promotes equal collaboration from all members.

## Gitlab Repo: https://gitlab.com/mrscottwlai/cs373-idb

## Gitlab Pipelines: https://gitlab.com/mrscottwlai/cs373-idb/-/pipelines

## Website Link: www.endangerednature.me

## Development Website Link: dev.d30vxib9rhm42v.amplifyapp.com

## Completion Times

### Phase I

| Name          | Estimated | Actual |
| ------------- | --------- | ------ |
| Scott Lai     | 13        | 13     |
| Colin Liu     | 7         | 10     |
| Victor Favela | 10        | 11     |
| Brian Wan     | 10        | 12     |
| Lucas Zimmer  | 8         | 9      |

### Phase II

| Name          | Estimated | Actual |
| ------------- | --------- | ------ |
| Scott Lai     | 10        | 14     |
| Colin Liu     | 12        | 12     |
| Victor Favela | 7         | 16     |
| Brian Wan     | 10        | 10     |
| Lucas Zimmer  | 10        | 10     |

### Phase III

| Name          | Estimated | Actual |
| ------------- | --------- | ------ |
| Scott Lai     | 9         | 18     |
| Colin Liu     | 10        | 10     |
| Victor Favela | 13        | 11     |
| Brian Wan     | 8         | 9      |
| Lucas Zimmer  | 10        | 10     |

### Phase IV

| Name          | Estimated | Actual |
| ------------- | --------- | ------ |
| Scott Lai     | 4         | 3      |
| Colin Liu     | 2         | 5      |
| Victor Favela | 4         | 4      |
| Brian Wan     | 3         | 2      |
| Lucas Zimmer  | 4         | 2      |

## Project Proposal
Endangered Nature is a environmental database which displays
information on endangered plants and animals and the regions they occupy in
the United States. It aims to bring awareness to these threatened species
and potentially highlight trends in their statistics.

## Data Sources
<!---
URLs of at least three disparate data sources that you will programmatically scrape using a RESTful API (be very sure about this)
-->
1. NatureServe Explorer
    * https://explorer.natureserve.org/
2. USDA Plants
    * https://plants.sc.egov.usda.gov/home
3. US Census
    * https://www.census.gov/data/developers/data-sets.html

## Models
<!---
* at least three models
* each model must connect to at least two other models
* an estimate of the number of instances of each model
-->
Species of Animalia | Species of Plantae | States
------------------- | ------------------ | ------
273                 | 176                | 50

## Sortable/Filterable Attributes
<!---
* each model must have many attributes
* describe at least five of those attributes for each model that you could filter by or sort by on the model (grid) pages
-->
Species of Animalia | Species of Plantae | States
------------------- | ------------------ | ----------------------
classification      | classification     | population
threat level        | threat level       | businesses
global status       | global status      | major industries
national status     | national status    | bordering states
state status        | state status       | educational attainment
states              | states             | land area
habitats            | habitats           | water area
length              | category           | _
weight              | duration           | _
population          | growth habit       | _
food habits         | _                  | _

## Searchable Attributes
<!---
* describe at least five additional attributes for each model that you could search for on the instance pages
-->
Species of Animalia | Species of Plantae | States
------------------- | ------------------ | ------------
scientific name     | scientific name    | name
common name         | common name        | nickname
name category       | name category      | abbreviation
plants              | animals            | capital
population trend    | economic uses      | major cities
_                   | _                  | plants
_                   | _                  | animals

## Media
<!---
* each model must be rich with different media (e.g., feeds, images, maps, text, videos, etc.) (be very sure about this)
* describe at least two types of media for each model that you could display on the instance pages
-->
Species of Animalia | Species of Plantae | States
------------------- | ------------------ | -------------------------
photo of animals    | photo of plants    | photo of environment
video of animals    | video of plants    | map of state

## Questions
<!---
what three questions will you answer due to doing this data synthesis on your site?
-->
1. Which states have the most amount of endangered animals?
3. Which habitats appear to be least threatened?
2. What industries have the most damaging impact on plants?

## Comments
* We used a decimal encoder from [this tutorial](https://bobbyhadz.com/blog/python-typeerror-object-of-type-decimal-is-not-json-serializable#:~:text=The%20Python%20%22TypeError%3A%20Object%20of%20type%20Decimal%20is,an%20example%20of%20how%20the%20error%20occurs.%20main.py).
* We used a pagination implementation from the [MUI docs](https://mui.com/components/pagination/).
* We used code from [this tutorial](https://github.com/forbesye/cs373/blob/main/Flask_AWS_Deploy.md) to deploy our Flask app to AWS.
* We manipulated code from [Univercity](https://gitlab.com/coleweinman/swe-college-project/) and [CatchingCongress](https://gitlab.com/catchingcongress/catchingcongress/) to implement searching, sorting, and filtering.