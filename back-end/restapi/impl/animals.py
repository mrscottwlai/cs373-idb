import json
import time
from database import (
    db,
    State,
    Border,
    City,
    Industry,
    Animal,
    FoodHabit,
    Habitat,
    AnimalHabitat,
    PlantHabitat,
    Plant,
    EconomicUse,
    AnimalState,
    PlantState,
)
from flask import request
from sqlalchemy.orm import selectinload
import common

# import cache


def AnimalToDict(animal, details):
    dictionary = common.object_as_dict(animal)
    dictionary["states"] = [
        {"id": animalState.state_id, "name": animalState.state.name}
        for animalState in animal.states
    ]
    dictionary["foodHabit"] = [
        {
            "foodHabit": foodHabit.habit,
            "adultHabit": foodHabit.adultHabit,
            "immatureHabit": foodHabit.immatureHabit,
        }
        for foodHabit in animal.foodHabits
    ]
    dictionary["habitats"] = [habitat.name for habitat in animal.habitats]
    if details:
        visitedPlants = set()
        dictionary["plants"] = []
        for animalState in animal.states:
            for plantState in animalState.state.plants:
                plant_id = plantState.plant_id
                if plant_id not in visitedPlants:
                    visitedPlants.add(plant_id)
                    dictionary["plants"].append(
                        {"id": plant_id, "name": plantState.plant.primaryCommonName}
                    )
    return dictionary


def GetAnimals():
    startTime = time.time()
    # attributes selected from a dropdown
    exact_filter_fields = [
        Animal.taxonomyClass,  # /animals?class=[taxonomyClass]?global=[globalStatus]?national=[nationalStatus]?threat=[threatLevel]
        Animal.globalStatus,
        Animal.nationalStatus,
        Animal.threatLevel
        # , Animal.states
    ]
    # attributes adjusted by a slider
    range_filter_fields = [
        Animal.length,  # /animals?lengthMin=[minimum length]&lengthMax=[maximum length]?weightMin=[minimum weight]&weightMax=[maximum weight]
        Animal.weight,
    ]
    # attributes to sort by
    sort_fields = [
        Animal.scientificName,  # /animals?sort=[scientific/common/length,weight]&ascending=[true/false]
        Animal.primaryCommonName,
        Animal.length,
        Animal.weight,
    ]
    # attributes to search by
    search_fields = [
        Animal.scientificName,  # /animals?q=[search query for the scientificName/primaryCommonName/nameCategory]
        Animal.primaryCommonName,
        Animal.nameCategory,
    ]
    # get dict of queries
    queries = request.args.to_dict(flat=False)
    # create BaseQuery
    animals = db.session.query(Animal).options(
        selectinload(Animal.states).selectinload(AnimalState.state),
        selectinload(Animal.foodHabits),
        selectinload(Animal.habitats),
    )
    # get page data from query
    page = common.get_query("page", queries)
    page = 1 if page == None else int(page[0])
    per_page = (
        int(common.get_query("perPage", queries).pop())
        if common.get_query("perPage", queries)
        else 20
    )
    # get search data from query
    query_field = common.parse_request_param(request.args, "q", str)
    # get filter data from query
    try:
        exact_filters = common.parse_exact_filters(
            request.args, Animal, exact_filter_fields
        )
        range_filters = common.parse_range_filters(
            request.args, Animal, range_filter_fields
        )
        sort_logic = common.parse_sort_fields(
            request.args, Animal, sort_fields, Animal.primaryCommonName
        )
    except Exception as e:
        return {"status": "fail", "data": e.args}
    # apply search data to BaseQuery
    if query_field is not None:
        animals = common.add_search_filters(animals, search_fields, query_field)
    # apply filter data to BaseQuery
    if len(exact_filters) > 0:
        animals = common.add_exact_filters(animals, exact_filters)
    if len(range_filters) > 0:
        animals = common.add_range_filters(animals, range_filters)
    # apply sorting to BaseQuery
    if sort_logic is None:
        animals = animals.order_by(Animal.primaryCommonName.desc())
    else:
        animals = animals.order_by(sort_logic)
    # paginate using given page queries
    animal_page = animals.paginate(page=page, per_page=per_page)
    # get instances from just one of the pages
    result = animal_page.items
    # format to JSON
    common.printTimeDebug("/animals before ToDict", startTime)
    outputList = [AnimalToDict(row, False) for row in result]
    # combine # of instances in query & a query page into a dict
    ret = {"count": animals.count(), "page": outputList}
    # print(ret)
    common.printTimeDebug("/animals", startTime)
    return json.dumps(ret, cls=common.DecimalEncoder, indent=4), 200


def GetAnimalId(options):
    """
    :param options: A dictionary containing all the paramters for the Operations
        options["animalId"]: The unique identifier of the animal

    """
    # another way of doing it
    # animal = db.session.query(Animal).filter_by(id=options["animalId"])
    # outputDict = AnimalToDict(animal.first_or_404())
    startTime = time.time()
    animal = (
        db.session.query(Animal)
        .options(
            selectinload(Animal.states)
            .selectinload(AnimalState.state)
            .selectinload(State.plants)
            .selectinload(PlantState.plant),
            selectinload(Animal.foodHabits),
            selectinload(Animal.habitats),
        )
        .get(options["animalId"])
    )
    outputDict = AnimalToDict(animal, True)
    common.printTimeDebug("/animals/id", startTime)
    return json.dumps(outputDict, cls=common.DecimalEncoder, indent=4), 200
