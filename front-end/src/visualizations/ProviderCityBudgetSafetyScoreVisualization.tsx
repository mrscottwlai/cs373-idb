import { useEffect, useState } from 'react';
import { Box, Typography, Grid, CircularProgress } from '@mui/material';
import {
    ScatterChart,
    Scatter,
    XAxis,
    YAxis,
    ZAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ResponsiveContainer,
  } from 'recharts';

const cached_data = [
    {name: 'Washington, D.C.' , b: 6 , s: 4 , r: 4.0 },
    {name: 'New York' , b: 8 , s: 4 , r: 4.30769 },
    {name: 'Jacksonville' , b: 5 , s: 4 , r: 3.66667 },
    {name: 'Miami' , b: 7 , s: 3 , r: 3.6 },
    {name: 'Atlanta' , b: 6 , s: 4 , r: 3.61111 },
    {name: 'Indianapolis' , b: 6 , s: 4 , r: 3.0 },
    {name: 'Wichita' , b: 4 , s: 4 , r: 4.0 },
    {name: 'Louisville' , b: 6 , s: 4 , r: 3.83333 },
    {name: 'Baltimore' , b: 7 , s: 3 , r: 3.47368 },
    {name: 'Kansas City' , b: 5 , s: 3 , r: 3.42857 },
    {name: 'Charlotte' , b: 6 , s: 4 , r: 3.57143 },
    {name: 'Raleigh' , b: 5 , s: 4 , r: 4.14286 },
    {name: 'Columbus' , b: 6 , s: 3 , r: 4.0 },
    {name: 'Oklahoma City' , b: 5 , s: 4 , r: 3.5 },
    {name: 'Tulsa' , b: 4 , s: 4 , r: 3.5 },
    {name: 'Philadelphia' , b: 7 , s: 4 , r: 3.77273 },
    {name: 'Memphis' , b: 5 , s: 4 , r: 3.0 },
    {name: 'Nashville' , b: 7 , s: 4 , r: 4.0 },
    {name: 'Arlington' , b: 6 , s: 4 , r: 2.0 },
    {name: 'Austin' , b: 7 , s: 4 , r: 4.27273 },
    {name: 'Dallas' , b: 6 , s: 4 , r: 3.57143 },
    {name: 'Fort Worth' , b: 6 , s: 4 , r: 3.9 },
    {name: 'Houston' , b: 6 , s: 3 , r: 2.92857 },
    {name: 'San Antonio' , b: 5 , s: 4 , r: 4.0 },
    {name: 'Virginia Beach' , b: 7 , s: 4 , r: 3.5 },
    {name: 'Chicago' , b: 7 , s: 4 , r: 4.36667 },
    {name: 'Boston' , b: 8 , s: 5 , r: 4.23529 },
    {name: 'Detroit' , b: 7 , s: 3 , r: 2.625 },
    {name: 'Minneapolis' , b: 7 , s: 4 , r: 3.88889 },
    {name: 'Omaha' , b: 5 , s: 4 , r: 3.66667 },
    {name: 'Milwaukee' , b: 6 , s: 4 , r: 3.25 },
    {name: 'Mesa' , b: 5 , s: 4 , r: 3.5 },
    {name: 'Phoenix' , b: 6 , s: 4 , r: 4.08333 },
    {name: 'Tucson' , b: 5 , s: 3 , r: 3.8 },
    {name: 'Bakersfield' , b: 5 , s: 4 , r: 3.0 },
    {name: 'Fresno' , b: 5 , s: 4 , r: 0.0 },
    {name: 'Long Beach' , b: 7 , s: 4 , r: 4.21429 },
    {name: 'Los Angeles' , b: 7 , s: 4 , r: 3.90244 },
    {name: 'Oakland' , b: 7 , s: 3 , r: 3.88235 },
    {name: 'Sacramento' , b: 6 , s: 4 , r: 3.375 },
    {name: 'San Diego' , b: 7 , s: 4 , r: 4.12 },
    {name: 'San Francisco' , b: 8 , s: 4 , r: 4.47368 },
    {name: 'San Jose' , b: 7 , s: 4 , r: 3.4 },
    {name: 'Colorado Springs' , b: 5 , s: 3 , r: 4.4 },
    {name: 'Denver' , b: 7 , s: 4 , r: 4.05882 },
    {name: 'Albuquerque' , b: 5 , s: 4 , r: 4.0 },
    {name: 'Las Vegas' , b: 6 , s: 4 , r: 3.82353 },
    {name: 'El Paso' , b: 4 , s: 4 , r: 3.33333 },
    {name: 'Portland' , b: 6 , s: 4 , r: 4.46154 },
    {name: 'Seattle' , b: 7 , s: 4 , r: 4.38462 },
]

const ProviderCityBudgetSafetyScoreVisualization = () => {
    const [data, setData] = useState<any[]>([]);
    const [loaded, setLoaded] = useState(true);

    useEffect(() => {
        if (!loaded){
            const tempData: any[] = [];
        }
        else {
            setData(cached_data)
        }
    }, []);

    return (
        <Box>
            <Typography gutterBottom variant="subtitle1" component="div" pt={2}>
            City Rating by budget rating distribution
            </Typography>
            <Grid container justifyContent="center">
                {loaded ? (
                    <ScatterChart width={1000} height={400}>
                        <CartesianGrid />
                        <XAxis type="number" dataKey="b" name="Budget" range={[3,9]} />
                        <YAxis type="number" dataKey="r" name="Rating" />
                        <ZAxis dataKey="name"/>
                        <Tooltip cursor={{ strokeDasharray: '3 3' }} />
                        <Scatter name="Cities" data={data} fill="#fd7e14" />
                    </ScatterChart>
                ) : (
                    <CircularProgress />
                )}
            </Grid>
        </Box>
    )
};

export default ProviderCityBudgetSafetyScoreVisualization;