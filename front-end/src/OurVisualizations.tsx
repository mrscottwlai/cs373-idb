import { Box, Typography } from '@mui/material';
import ExampleVisualization from './visualizations/ExampleVisualization';
import StateSpeciesDistributionVisualization from './visualizations/StateSpeciesDistributionVisualization';
import AnimalSizeLengthVisualization from './visualizations/AnimalSizeLengthVisualization';
import PlantEconomicUsesVisualization from './visualizations/PlantEconomicUsesVisualization';

const OurVisualizations = () => {
    return (
      <Box>
        <Typography gutterBottom variant="h5" component="div" pt={2}>
          Our Visualizations
        </Typography>
        <StateSpeciesDistributionVisualization/>
        <AnimalSizeLengthVisualization/>
        <PlantEconomicUsesVisualization/>
      </Box>
    );
  };
  
  export default OurVisualizations;
  