import React from "react";
import { Link } from 'react-router-dom';
import { withStyles } from "@material-ui/core/styles";

import "bootstrap/dist/css/bootstrap.min.css";
import "./Home.css";
import { Container,
  Grid,
  Typography,
  Card,
  CardContent,
  CardMedia,
  CardActionArea,
  Box,
} from '@mui/material';
import Image from './../assets/home-images/forest0.jpg';
import AnimalImage from './../assets/home-images/coyote.jpeg';
import PlantImage from './../assets/home-images/plant.jpeg';
import StatesImage from './../assets/home-images/states.jpeg';

const styles = {
  splashImage: {
    backgroundImage: `url(${Image})`,
    height: '130vh',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    position: 'relative',
  },

  splashText: {
    textAlign: 'center',
    position: 'absolute',
    top: '30%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    textShadow: '1px 1px 2px black, 0 0 1em blue, 0 0 0.2em blue',
  },

  splashCard: {
    backgroundPosition: 'bottom',
    top: '50%',
    left: '50%',
    backgroundColor: 'rgba(52, 52, 52, 0.0)',
    transform: 'translate(0, 150%)',

  },

  cardBottom: {
    backgroundColor: 'rgba(52, 52, 52, 0.1)'
  }
} as const;

const TextTypography = withStyles({
  root: {
    color: "white"
  }
})(Typography);

function Home() {

    return (

        <Box style={styles.splashImage}>
            <Box >
                <Box style={styles.splashText}>
                <TextTypography id="motto" variant="h3">
                    The United States has more than 1300 species of plants and animals classified as endangered or at risk
                </TextTypography>
                <Box style={{ width: '10px', height: '10px' }}></Box>
                <TextTypography id="tagline" variant="h6">
                    Learn more about endangered species in your state!
                </TextTypography>
                <Box style={{ width: '40px', height: '50px' }}></Box>
                </Box>
            </Box>

            <Box style={styles.splashCard}>
                <Container>
                    <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 2, sm: 8, md: 12 }} p={3}>
                    <Grid item xs={2} sm={4} md={4}>
                        <Link to="/animals" style={{ textDecoration: 'none' }}>
                        <Card id="animalsCard" sx={{ maxWidth: 345 }}>
                            <CardActionArea style={{ outline: 'none' }}>
                            <CardMedia component="img" height="200" image={AnimalImage} />
                            <CardContent style={styles.cardBottom}>
                                <Typography variant="h5" component="div">
                                Animals
                                </Typography>
                                <Box sx={{ minHeight: '7vh' }}>
                                <Typography variant="body2" color="text.secondary">
                                    Discover animal species at risk within the UnitedStates
                                </Typography>
                                </Box>
                            </CardContent>
                            </CardActionArea>
                        </Card>
                        </Link>
                    </Grid>

                    <Grid item xs={2} sm={4} md={4}>
                        <Link to="/plants" style={{ textDecoration: 'none' }}>
                        <Card id="plantsCard" sx={{ maxWidth: 345 }}>
                            <CardActionArea style={{ outline: 'none' }}>
                            <CardMedia component="img" height="200" image={PlantImage} />
                            <CardContent style={styles.cardBottom}>
                                <Typography variant="h5" component="div">
                                Plants
                                </Typography>
                                <Box sx={{ minHeight: '7vh' }}>
                                <Typography variant="body2" color="text.secondary">
                                    Discover plants species at risk. More than 700 species of plants are endangered.
                                </Typography>
                                </Box>
                            </CardContent>
                            </CardActionArea>
                        </Card>
                        </Link>
                    </Grid>

                    <Grid item xs={2} sm={4} md={4}>
                        <Link to="/states" style={{ textDecoration: 'none' }}>
                        <Card id="statesCard" sx={{ maxWidth: 345 }}>
                            <CardActionArea style={{ outline: 'none' }}>
                            <CardMedia component="img" height="200" image={StatesImage} />
                            <CardContent style={styles.cardBottom}>
                                <Typography variant="h5" component="div">
                                States
                                </Typography>
                                <Box sx={{ minHeight: '7vh' }}>
                                <Typography variant="body2" color="text.secondary">
                                    Find out which species are most endangered by state as well as State statistics relating to endangered species.
                                </Typography>
                                </Box>
                            </CardContent>
                            </CardActionArea>
                        </Card>
                        </Link>
                    </Grid>
                    </Grid>
                </Container>
                </Box>

        </Box>

    );
}

export default Home;
