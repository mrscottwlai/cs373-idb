import { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import {
  Typography,
  Box,
  Container,
  Card,
  CardContent,
  CardMedia,
  CardActionArea,
  CircularProgress,
  List,
  ListItem,
  ListItemText,
  styled,
  Paper
} from '@mui/material';
import { Row } from "react-bootstrap";
import { getAnimal } from '../../APIFunctions';

const StyledCardActionArea = styled(CardActionArea)(({theme}) => `
    .MuiCardActionArea-focusHighlight {
        background: transparent;
    }
`);

const Animal = () => {
  const animal_id = useParams().id || '';

  const [animalData, setAnimalData] = useState<any>([]);
  const [loadedData, setLoadedData] = useState(false);

  useEffect(() => {
    getAnimal(animal_id).then((response: any) => {
      setAnimalData(response);
      setLoadedData(true);
    });
  }, [animal_id]);

  let content = null;
  if (loadedData) {
    // console.log(animalData);
    content = (
      <div className="About">
        <Container>
        <Row sx={{paddingBottom: '10%'}}>
          <Box sx={{ width: '35%' }}>
            <div className="DivLeft">
              <Typography sx={{ fontWeight: 'bold', fontSize: 'h2.fontSize'}}>{animalData.primaryCommonName}</Typography>
              <Typography sx={{ fontWeight: 'medium', fontSize: 'h4.fontSize', fontStyle: 'italic', paddingBottom:2 }}>{animalData.scientificName}</Typography>
            </div>


          </Box>

          <Box sx={{ width: '65%', paddingTop: '2%'}}>
              <div className="DivRight">
              <Typography sx={{ fontWeight: 'bold', fontSize: 'h3.fontSize'}}>Threat Level: {animalData.threatLevel}</Typography>
            </div>
          </Box>
        </Row>
        <hr/>
        <Row sx={{padding:'10%'}}>
          <Box sx={{ width: '100%', padding: '7%'}}>

            <Card variant="outlined">
              <CardActionArea style={{ outline: 'none' }}>
                <td dangerouslySetInnerHTML={{__html: animalData.video}} />
              </CardActionArea>
            </Card>
          </Box>
        </Row>
        <hr/>

        <Row>
          <Box sx={{width:'50%', paddingTop:'10%'}}>
            <Card variant="outlined">
              <StyledCardActionArea style={{ outline: 'none' }}>
                <CardContent sx={{ textAlign: 'left', }}>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h3.fontSize'}}>Animal Info</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Population: {animalData.population}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Population Trend: {animalData.populationTrend}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Animal Length: {animalData.length} cm</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Animal Weight: {animalData.weight} g</Typography>
                </CardContent>
              </StyledCardActionArea>
            </Card>
            <Card variant="outlined">
              <StyledCardActionArea style={{ outline: 'none' }}>
                <CardContent sx={{ textAlign: 'left', }}>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h3.fontSize'}}>Classification</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Name Category: {animalData.nameCategory}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Kingdom: {animalData.taxonomyKingdom}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Phylum: {animalData.taxonomyPhylum}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Class: {animalData.taxonomyClass}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Order: {animalData.taxonomyOrder}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Family: {animalData.taxonomyFamily}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Genus: {animalData.taxonomyGenus}</Typography>
                </CardContent>
              </StyledCardActionArea>
            </Card>

            <Card variant="outlined">
              <StyledCardActionArea style={{ outline: 'none' }}>
                <CardContent sx={{ textAlign: 'left', }}>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h3.fontSize'}}>Animal Status</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Threat Level: {animalData.threatLevel}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Global Status: {animalData.globalStatus}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>National Status: {animalData.nationalStatus}</Typography>
                </CardContent>
              </StyledCardActionArea>
            </Card>
            <Card variant="outlined">
                <StyledCardActionArea style={{ outline: 'none' }}>
                  <div className="DivCenter">
                    Habitats
                    <ul>
                      {animalData.habitats.map((habitat: string, i: number) => {
                        return (
                        <Typography key={i} sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>{habitat}</Typography>
                        );
                      })}
                  </ul>
                  </div>
                </StyledCardActionArea>
              </Card>
              <Card variant="outlined">
                <StyledCardActionArea style={{ outline: 'none' }}>
                  <div className="DivCenter">
                    Food Habits
                    <ul>
                      {animalData.foodHabit.map((foodHabit: {foodHabit: string}, i: number) => {
                        return (
                        <Typography key={i} sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>{foodHabit.foodHabit}</Typography>
                        );
                      })}
                  </ul>
                  </div>
                </StyledCardActionArea>
              </Card>
          </Box>
          <Box sx={{width:'50%', paddingTop:'10%'}}>
            <Card variant="outlined">
              <StyledCardActionArea style={{ outline: 'none' }}>
                <CardMedia component="img" image={animalData.image}  />
              </StyledCardActionArea>
            </Card>

          </Box>

        </Row>
        <Row>
          <p> </p>
        </Row>
        <br />


        <Row padding="5">
            <Box sx={{width:'50%'}}>

              <Card variant="outlined">
                <StyledCardActionArea style={{ outline: 'none' }}>
                  <div className="DivCenter">
                    Plants in the same state
                    <Paper style={{maxHeight: 200, overflow: 'auto'}}>
                    <List>
                      {animalData.plants.map((plant: {name: string, id:string}, i: number) => {
                        return (
                          <ListItem key={i} button component={Link} to={"/plants/"+plant.id}>
                            <ListItemText primary={plant.name} />
                          </ListItem>
                        );
                      })}
                    </List>
                    </Paper>

                  </div>
                </StyledCardActionArea>
              </Card>
            </Box>
            <Box sx={{width:'50%'}}>
              <Card variant="outlined">
                <StyledCardActionArea style={{ outline: 'none' }}>
                  <div className="DivCenter">
                    States
                    <Paper style={{maxHeight: 200, overflow: 'auto'}}>
                    <List>
                      {animalData.states.map((state: {name: string, id:string}, i: number) => {
                        return (
                          <ListItem key={i} button component={Link} to={"/states/"+state.id}>
                            <ListItemText primary={state.name} />
                          </ListItem>
                        );
                      })}
                    </List>
                    </Paper>

                  </div>
                </StyledCardActionArea>
              </Card>
            </Box>
        </Row>
        <br />

      </Container>
    </div>
    );
  }

  return <Box>{loadedData ? content : <CircularProgress />}</Box>;
};

export default Animal;
