import { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import {
  Typography,
  Box,
  Container,
  Card,
  CardContent,
  CardMedia,
  CardActionArea,
  CircularProgress,
  List,
  ListItem,
  ListItemText,
  styled,
  Paper
} from '@mui/material';
import { getState } from '../../APIFunctions';
import { Row } from 'react-bootstrap';

const StyledCardActionArea = styled(CardActionArea)(({theme}) => `
    .MuiCardActionArea-focusHighlight {
        background: transparent;
    }
`);

const State = () => {
  const state_id = useParams().id || '';

  const [stateData, setStateData] = useState<any>([]);
  const [loadedData, setLoadedData] = useState(false);

  useEffect(() => {
    getState(state_id).then((response: any) => {
      setStateData(response);
      setLoadedData(true);
    });
  }, []);

    let content = null;
  if (loadedData) {
    content = (
      <div className="About">
        <Container>
        <Row sx={{paddingBottom: '10%'}}>
          <Box sx={{ width: '35%' }}>
            <div className="DivLeft">
              <Typography sx={{ fontWeight: 'bold', fontSize: 'h2.fontSize'}}>{stateData.name}</Typography>
              <Typography sx={{ fontWeight: 'medium', fontSize: 'h4.fontSize', fontStyle: 'italic', paddingBottom:2 }}>{stateData.nickname}</Typography>
            </div>


          </Box>

          <Box sx={{ width: '65%', paddingTop: '2%'}}>
              <div className="DivRight">
              <Typography sx={{ fontWeight: 'bold', fontSize: 'h3.fontSize'}}></Typography>
            </div>
          </Box>
        </Row>
        <hr/>
        <Row sx={{padding:'10%'}}>
          <Box sx={{ width: '100%', padding: '7%'}}>

          <iframe src={stateData.map} width="900" height="450"></iframe>
          </Box>
        </Row>


        <br />
         <Row padding="5">
            <Box sx={{width:'50%'}}>
              <Card variant="outlined">
                <StyledCardActionArea style={{ outline: 'none' }}>
                  <div className="DivCenter">
                    Animals
                    <Paper style={{maxHeight: 200, overflow: 'auto'}}>

                    <List>
                      {stateData.animals.map((animal: {name: string, id:string}, i: number) => {
                        return (
                          <ListItem key={i} button component={Link} to={"/animals/"+animal.id}>
                            <ListItemText primary={animal.name} />
                          </ListItem>
                        );
                      })}
                    </List>
                    </Paper>

                  </div>
                </StyledCardActionArea>
              </Card>
            </Box>
            <Box sx={{width:'50%'}}>
              <Card variant="outlined">
                <StyledCardActionArea style={{ outline: 'none' }}>
                  <div className="DivCenter">
                    Plants
                    <Paper style={{maxHeight: 200, overflow: 'auto'}}>
                    <List>
                      {stateData.plants.map((plant: {name: string, id:string}, i: number) => {
                        return (
                          <ListItem key={i} button component={Link} to={"/plants/"+plant.id}>
                            <ListItemText primary={plant.name} />
                          </ListItem>
                        );
                      })}
                    </List>
                    </Paper>

                  </div>
                </StyledCardActionArea>
              </Card>
            </Box>
        </Row>


        <br />


        <Row>
          <Box sx={{width:'50%'}}>
            <Card variant="outlined">
              <StyledCardActionArea style={{ outline: 'none' }}>
                <CardContent sx={{ textAlign: 'left', }}>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h3.fontSize'}}>State Info</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Capital: {stateData.capital}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Population: {stateData.population}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Land Area: {stateData.landArea} sq mi</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Water Area: {stateData.waterArea} sq mi</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Businesses: {stateData.businessNumber}</Typography>
                  <Typography sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>Education Attainment: {stateData.educationalAttainment}%</Typography>


                </CardContent>
              </StyledCardActionArea>
            </Card>
            <Card variant="outlined">
                <StyledCardActionArea style={{ outline: 'none' }}>
                  <div className="DivCenter">
                    Major Cities
                    <ul>
                      {stateData.majorCities.map((cities: string, i: number) => {
                        return (
                        <Typography key={i} sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>{cities}</Typography>
                        );
                      })}
                  </ul>
                  </div>
                </StyledCardActionArea>
              </Card>
            <Card variant="outlined">
                <StyledCardActionArea style={{ outline: 'none' }}>
                  <div className="DivCenter">
                    Major Industries
                    <ul>
                      {stateData.majorIndustries.map((industry: string, i: number) => {
                        return (
                        <Typography key={i} sx={{ fontWeight: 'bold', fontSize: 'h6.fontSize'}}>{industry}</Typography>
                        );
                      })}
                  </ul>
                  </div>
                </StyledCardActionArea>
              </Card>
          </Box>
          <Box sx={{width:'50%'}}>
            <Card variant="outlined">
              <StyledCardActionArea style={{ outline: 'none' }}>
                <CardMedia component="img" image={stateData.image}  />
              </StyledCardActionArea>
            </Card>
            <br />
            <br />
            <Card variant="outlined">
              <StyledCardActionArea style={{ outline: 'none' }}>
                <CardMedia component="img" image={stateData.flag}  />
              </StyledCardActionArea>
            </Card>

          </Box>

        </Row>
        <Row>
          <p> </p>
        </Row>



      </Container>
    </div>
    );
  }

  return <Box>{loadedData ? content : <CircularProgress />}</Box>;
};

export default State;
