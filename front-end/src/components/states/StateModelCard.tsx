import { Link } from 'react-router-dom';
import { Grid, Typography, Card, CardContent, CardMedia, CardActionArea } from '@mui/material';
import { Highlight } from 'react-highlighter-ts';
const styles = {
  card: {
    height: '100%',
    width: '95%',
    margin: 'auto',
  },

  media: {
    height: 280,
    width: '100%',
    objectFit: 'cover',
  },
} as const;

const StateModelCard = (props: any) => {
  const { state_id, name, pop, landArea, waterArea, abbreviation, educationalAttainment, businessNumber, image, highlightSearch} =
    props;

  return (
    <Grid item key={state_id} xs={1} sm={4} md={3}>
      <Link to={`/states/${state_id}`} style={{ textDecoration: 'none' }}>
        <Card sx={{ maxWidth: 345 }} style={styles.card}>
          <CardActionArea style={{ outline: 'none' }}>
            <CardMedia component="img" image={image} style={styles.media} />
            <CardContent sx={{ textAlign: 'left'}}>
              <Typography sx={{ fontWeight: 'bold', fontSize: 'h7.fontSize'}}>
                <Highlight search = {highlightSearch}>
                  {name}
                </Highlight>
              </Typography>
              <Typography sx={{ fontWeight: 'medium', fontStyle: 'italic', paddingBottom:2 }}>
                <Highlight search = {highlightSearch}>
                  {abbreviation}
                </Highlight>
              </Typography>
              <Typography sx={{fontSize: 'caption.fontSize'}}>
                <Highlight search = {highlightSearch}>
                  Population: {pop}
                </Highlight>
              </Typography>
              <Typography sx={{fontSize: 'caption.fontSize'}}>
                <Highlight search = {highlightSearch}>
                  Education: {educationalAttainment}%
                </Highlight>
              </Typography>
              <Typography sx={{fontSize: 'caption.fontSize'}}>
                <Highlight search = {highlightSearch}>
                  Businesses: {businessNumber}
                </Highlight>
              </Typography>
              <Typography sx={{fontSize: 'caption.fontSize'}}>
                <Highlight search = {highlightSearch}>
                  Land Area: {landArea} sq mi
                </Highlight>
              </Typography>
              <Typography sx={{fontSize: 'caption.fontSize'}}>
                <Highlight search = {highlightSearch}>
                  Water Area: {waterArea} sq mi
                </Highlight>
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      </Link>
    </Grid>
  );
};

export default StateModelCard;
