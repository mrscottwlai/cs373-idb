import { useEffect, useState } from 'react';
import { Box, Typography, Grid, CircularProgress } from '@mui/material';
import { PieChart, Pie, Sector, Cell, ResponsiveContainer, Tooltip, Legend, } from 'recharts';

const cached_data = [
{ name: 'Logistics & Warehouse Jobs' , value: 377 },
{ name: 'Teaching Jobs' , value: 109 },
{ name: 'Travel Jobs' , value: 96 },
{ name: 'Sales Jobs' , value: 89 },
{ name: 'Healthcare & Nursing Jobs' , value: 47 },
{ name: 'Admin Jobs' , value: 37 },
{ name: 'IT Jobs' , value: 36 },
{ name: 'Hospitality & Catering Jobs' , value: 34 },
{ name: 'Customer Services Jobs' , value: 25 },
{ name: 'Accounting & Finance Jobs' , value: 21 },
{ name: 'Other' , value: 21 },
{ name: 'Engineering Jobs' , value: 19 },
{ name: 'Maintenance Jobs' , value: 19 },
{ name: 'Trade & Construction Jobs' , value: 16 },
{ name: 'Energy, Oil & Gas Jobs' , value: 14 },
{ name: 'PR, Advertising & Marketing Jo' , value: 11 },
{ name: 'HR & Recruitment Jobs' , value: 10 },
{ name: 'Creative & Design Jobs' , value: 7 },
{ name: 'Manufacturing Jobs' , value: 7 },
{ name: 'Retail Jobs' , value: 5 },
]

const ProviderJobDistributionVisualization = () => {
    const [data, setData] = useState<any[]>([]);
    const [loaded, setLoaded] = useState(true);

    let renderLabel = function(entry:any) {
        return entry.name;
    }

    useEffect(() => {
        if (!loaded){
            const tempData: any[] = [];
        }
        else {
            setData(cached_data)
        }
    }, []);

    return (
        <Box>
            <Typography gutterBottom variant="subtitle1" component="div" pt={2}>
            Job Category Distribution
            </Typography>
            <Grid container justifyContent="center">
                {loaded ? (
                    <PieChart width={1000} height={500}>
                        <Pie
                            data={data}
                            dataKey="value"
                            nameKey="name"
                            fill="#fd7e14"
                            legendType = "line"
                            label = {renderLabel}
                        />
                        <Tooltip />
                  </PieChart>
        
                ) : (
                    <CircularProgress />
                )}
            </Grid>
        </Box>

    );

};

export default ProviderJobDistributionVisualization;