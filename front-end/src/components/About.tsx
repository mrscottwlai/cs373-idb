import React, { useEffect, useState, FC } from "react";
import { Container, Card, Row, Col, Accordion, Button} from "react-bootstrap";
import { Card as AntdCard } from "antd";
import { GitlabOutlined } from "@ant-design/icons";
import axios from "axios";

import emptyheadshot from "../assets/member-images/EmptyHeadshot.png";
import scott_headshot from "../assets/member-images/Scott_Headshot.png";
import colin_headshot from "../assets/member-images/Colin_Headshot.jpeg";
import victor_headshot from "../assets/member-images/Victor_Headshot.jpeg";
import brian_headshot from "../assets/member-images/Brian_Headshot.jpeg";
import lucas_headshot from "../assets/member-images/Lucas_Headshot.jpeg";

import natureserve from "../assets/tool-images/NatureServe.png";
import usdaplants from "../assets/tool-images/USDAPlants.jpg";
import census from "../assets/tool-images/Census.png";

import react_img from "../assets/tool-images/react.png";
import reactbootstrap from "../assets/tool-images/reactbootstrap.png";
import reactrouter from "../assets/tool-images/reactrouter.jpg";
import prettier from "../assets/tool-images/prettier.png";
import eslint from "../assets/tool-images/eslint.png";
import awsamplify from "../assets/tool-images/awsamplify.png";
import gitlab from "../assets/tool-images/gitlab.png";
import discord from "../assets/tool-images/discord.jpg";

import { Box, Typography } from '@mui/material';

import "bootstrap/dist/css/bootstrap.min.css";
import "./About.css";

const { Meta } = AntdCard;

class Developer {
  name: string;
  username: string;
  commits: number;
  issues: number;
  tests: number;
  responsibility: string;
  bio: string;
  photo: string;

  constructor(
    name: string,
    username: string,
    responsibility: string,
    bio: string,
    photo: string
  ) {
    this.name = name;
    this.username = username;
    this.commits = 0;
    this.issues = 0;
    this.tests = 0;
    this.responsibility = responsibility;
    this.bio = bio;
    this.photo = photo;
  }
}

interface DevProps {
  dev: Developer;
}

const DevCard: FC<DevProps> = (props) => {
  return (
    <AntdCard
      bordered={true}
      cover={<img className="circle_headshot" src={props.dev.photo} alt={""} />}
      className="dev_card"
      // Match height of other components
      bodyStyle={{
        alignItems: "stretch",
        height: "100%",
        width: "100%",
        display: "flex",
        flexDirection: "column",
      }}
    >
      <p> </p>
      <h3>
        <strong>{props.dev.name}</strong>
      </h3>
      <div className="gitStats">
        <div className="gitStat">
          <div>
            <strong>{props.dev.commits}</strong>
          </div>
          <div>Commits</div>
          <p> </p>
        </div>
        <div className="gitStat">
          <div>
            <strong>{props.dev.issues}</strong>
          </div>
          <div>Issues</div>
          <p> </p>
        </div>
        <div className="gitStat">
          <div>
            <strong>{props.dev.tests}</strong>
          </div>
          <div>Tests</div>
          <p> </p>
        </div>
      </div>
      <p> </p>
      <Meta
        title={<strong>{props.dev.responsibility}</strong>}
        description={props.dev.bio}
        className="meta"
      />
      <p> </p>
    </AntdCard>
  );
};

interface StatsProps {
  commits: number;
  issues: number;
  tests: number;
}

const StatsCard: FC<StatsProps> = (props) => {
  return (
    <AntdCard
      bordered={true}
      className="stats_card"
      // Match height of other components
      bodyStyle={{
        alignItems: "stretch",
        height: "100%",
        display: "flex",
        flexDirection: "column",
      }}
    >
      <p> </p>
      <p> </p>
      <h1 className="title" color="black">Statistics Overview</h1>
      <p> </p>
      <Row>
        <Col>
          <strong>
            <GitlabOutlined style={{ fontSize: "3em" }} />
          </strong>
        </Col>
      </Row>
      <p> </p>
      <p> </p>
      <h2>{props.commits}</h2>
      <h5>Commits</h5>
      <h2>{props.issues}</h2>
      <h5>Issues</h5>
      <h2>{props.tests}</h2>
      <h5>Tests</h5>
    </AntdCard>
  );
};

class Tool {
  name: string;
  description: string;
  link: string;
  photo: string;

  constructor(name: string, description: string, link: string, photo: string) {
    this.name = name;
    this.description = description;
    this.link = link;
    this.photo = photo;
  }
}

interface ToolProps {
  tool: Tool;
}

const ToolCard: FC<ToolProps> = (props) => {
  return (
    <Col key="Name" style={{ padding: 30 }}>
      <Card className="tool_card">
        <Card.Img
          variant="bot"
          className="about-img"
          src={props.tool.photo}
          style={{
            height: 175,
            maxWidth: 275,
          }}
        />
        <Card.Body>
          <Card.Title>{props.tool.name}</Card.Title>
          <Card.Text>{props.tool.description}</Card.Text>
          <Button variant="outline-dark" href={props.tool.link} target="_blank">
            More Info
          </Button>
        </Card.Body>
      </Card>
    </Col>
  );
};

const devInfo = [
  new Developer(
    "Scott Lai",
    "mrscottwlai",
    "Full Stack",
    "I'm a junior studying computer science at UT Austin. I'm from Houston, TX, and I enjoy coding, gaming, and video editing.",
    scott_headshot
  ),
  new Developer(
    "Colin Liu",
    "speedycatfish",
    "Backend",
    "I'm a junior studying computer science from Portland, Oregon, and I enjoy playing soccer.",
    colin_headshot
  ),
  new Developer(
    "Victor Favela",
    "VictorFavela",
    "Full Stack",
    "I'm a senior studying computer science and economics at UT Austin.",
    victor_headshot
  ),
  new Developer(
    "Brian Wan",
    "wanbrian",
    "Frontend",
    "I'm a senior computer science major from Katy, Texas.",
    brian_headshot
  ),
  new Developer(
    "Lucas Zimmer",
    "lcz0103",
    "Backend",
    "I'm a junior computer science major from McKinney, Texas, and I enjoy running.",
    lucas_headshot
  ),
];

const names_dict: { [name: string]: number } = {
  "Scott Lai": 0,
  "Colin Liu": 1,
  "Victor Favela": 2,
  "brian Wan": 3,
  "Lucas Zimmer": 4,
};

const toolInfo = [
  new Tool(
    "Default",
    "description",
    "https://explorer.natureserve.org/",
    emptyheadshot
  ),

  new Tool(
    "NatureServe Explorer",
    "information on rare and endangered species and ecosystems in the Americas",
    "https://explorer.natureserve.org/",
    natureserve
  ),
  new Tool(
    "USDA Plants",
    "standardized information about the vascular plants, mosses, liverworts, hornworts, and lichens of the U.S. and its territories",
    "https://plants.sc.egov.usda.gov/home",
    usdaplants
  ),
  new Tool(
    "US Census",
    "Demographic, economic and population data from the U.S. Census Bureau",
    "https://www.census.gov/data/developers/data-sets.html",
    census
  ),

  new Tool(
    "React",
    "Javascript library used to build frontend",
    "https://reactjs.org",
    react_img
  ),
  new Tool(
    "React-Bootstrap",
    "Provided react components for frontend",
    "https://react-bootstrap.github.io/",
    reactbootstrap
  ),
  new Tool(
    "React-Router",
    "Used to implement navigation between pages",
    "https://reactrouter.com",
    reactrouter
  ),
  new Tool("Prettier", "Code formatter", "https://prettier.io", prettier),
  new Tool("Eslint", "Linter", "https://eslint.org", eslint),
  new Tool(
    "AWS Amplify",
    "Hosting service for website",
    "https://aws.amazon.com/amplify",
    awsamplify
  ),
  new Tool(
    "Gitlab",
    "Version control and Issue tracker",
    "https://about.gitlab.com",
    gitlab
  ),
  new Tool("Discord", "Team communication", "https://discord.com", discord),
];

function About() {
  const [baseUserData] = useState(devInfo);

  const [issueData, setIssueData] = useState(0);
  const [commitData, setCommitData] = useState(0);

  useEffect(() => {
    const getIssues = async () => {
      let issues = 0;
      Promise.all(
        baseUserData.map(async (dev) => {
          await axios
            .get(
              `https://gitlab.com/api/v4/projects/39651560/issues_statistics?author_username=${dev.username}`
            )
            .then((response) => response.data)
            .then((data) => {
              dev.issues = data.statistics.counts.all;
              issues += dev.issues;
            });
        })
      ).then(() => {
        setIssueData(issues);
      });
    };
    getIssues();
  }, [baseUserData]);

  useEffect(() => {
    const getCommits = async () => {
      let commit_list = [0, 0, 0, 0, 0];
      let commit_count = 0;
      let commits_response = await axios.get(
        "https://gitlab.com/api/v4/projects/39651560/repository/commits"
      );
      let commits = commits_response.data;

      for (let i = 0; i < commits.length; i++) {
        let cur_name: string = commits[i].committer_name;
        let cur_member: number = names_dict[cur_name];
        if (cur_member != null) {
          commit_list[cur_member] += 1;
        }
        commit_count += 1;
      }

      setCommitData(commit_count);

      for (let i = 0; i < 5; i++) {
        devInfo[i].commits = commit_list[i];
      }
    };
    getCommits();
  }, [commitData]);

  return (
    <div className="About">
      <Container>
        <Row sx={{paddingBottom: '10%'}}>
          <Box sx={{ width: '35%' }}>
            <div className="DivLeft">
              About
            </div>
          </Box>
          <Box sx={{ width: '65%',   paddingTop: '2%', paddingBottom: '5%'}}>
            <Typography variant='body1' align="left">
              EndangeredNature is a environmental database which displays
              information on endangered plants and animals and the regions
              they occupy in the United States. It aims to bring awareness to
              these threatened species and potentially highlight trends in
              their statistics.
            </Typography>
            <Accordion flush alwaysOpen >
            <Accordion.Item eventKey="2">
              <Accordion.Header>Integration</Accordion.Header>
              <Accordion.Body>
                By connecting the information about both endangered animals and
                plants to states, we are able to encourage users to look into
                how they can help protect these species in their own backyard.
                Integrating these concepts allows us to remove the idea that
                these animals exist "somewhere" and puts them at the forefront
                of the users mind.
              </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="3">
              <Accordion.Header>Documentation</Accordion.Header>
              <Accordion.Body>
                <Accordion.Body>
                  <Button
                    href="https://app.swaggerhub.com/apis-docs/speedycatfishex/endangered-nature/"
                    target="_blank"
                  >
                    API documentation
                  </Button>
                  <Button
                    href="https://gitlab.com/mrscottwlai/cs373-idb"
                    target="_blank"
                  >
                    Gitlab Source code
                  </Button>
                </Accordion.Body>
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
          </Box>
        </Row>
        <hr/>

        <div className="DivRight">
              Meet the Team
            </div>
        <Row>
          <p> </p>
        </Row>
        <Row>
          <div className="col">
            <DevCard dev={devInfo[0]} />
          </div>
          <div className="col">
            <DevCard dev={devInfo[1]} />
          </div>
          <div className="col">
            <DevCard dev={devInfo[2]} />
          </div>
        </Row>
        <Row>
          <p> </p>
        </Row>
        <Row>
          <div className="col">
            <DevCard dev={devInfo[3]} />
          </div>
          <div className="col">
            <DevCard dev={devInfo[4]} />
          </div>
          <div className="col">
            <StatsCard commits={commitData} issues={issueData} tests={0} />
          </div>
        </Row>
        <br />
        <hr/>

        <div className="DivLeft">
              Data Sources and APIs
            </div>
        <Row padding="5">
          <ToolCard tool={toolInfo[1]} />
          <ToolCard tool={toolInfo[2]} />
          <ToolCard tool={toolInfo[3]} />
        </Row>
        <br />
         <hr/>

        <div className="DivRight">
              Tools and Frameworks
            </div>
        <br />
        <Row>
          <ToolCard tool={toolInfo[4]} />
          <ToolCard tool={toolInfo[5]} />
          <ToolCard tool={toolInfo[6]} />
          <ToolCard tool={toolInfo[7]} />
          <ToolCard tool={toolInfo[8]} />
          <ToolCard tool={toolInfo[9]} />
          <ToolCard tool={toolInfo[10]} />
          <ToolCard tool={toolInfo[11]} />
        </Row>
      </Container>
    </div>
  );
}

export default About;
