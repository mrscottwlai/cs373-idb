import { useEffect, useState } from 'react';
import { Box, Typography, Grid, CircularProgress } from '@mui/material';
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const cached_data = [
    {name: 'Chicago' , apartments: 6 , singlefamilys: 2 , condos: 2 , townhouses: 0 },
    {name: 'Baltimore' , apartments: 4 , singlefamilys: 1 , condos: 1 , townhouses: 4 },
    {name: 'Boston' , apartments: 10 , singlefamilys: 0 , condos: 0 , townhouses: 0 },
    {name: 'Virginia Beach' , apartments: 1 , singlefamilys: 6 , condos: 0 , townhouses: 3 },
    {name: 'Charlotte' , apartments: 0 , singlefamilys: 10 , condos: 0 , townhouses: 0 },
    {name: 'Atlanta' , apartments: 1 , singlefamilys: 9 , condos: 0 , townhouses: 0 },
    {name: 'Washington, DC' , apartments: 7 , singlefamilys: 2 , condos: 0 , townhouses: 1 },
    {name: 'Albuquerque' , apartments: 2 , singlefamilys: 7 , condos: 0 , townhouses: 1 },
    {name: 'Bakersfield' , apartments: 0 , singlefamilys: 10 , condos: 0 , townhouses: 0 },
    {name: 'Los Angeles' , apartments: 10 , singlefamilys: 0 , condos: 0 , townhouses: 0 },
    {name: 'Nashville' , apartments: 0 , singlefamilys: 9 , condos: 1 , townhouses: 0 },
    {name: 'Louisville' , apartments: 0 , singlefamilys: 9 , condos: 1 , townhouses: 0 },
    {name: 'Houston' , apartments: 10 , singlefamilys: 0 , condos: 0 , townhouses: 0 },
    {name: 'Fort Worth' , apartments: 0 , singlefamilys: 10 , condos: 0 , townhouses: 0 },
    {name: 'Colorado Springs' , apartments: 2 , singlefamilys: 7 , condos: 0 , townhouses: 1 },
    {name: 'San Antonio' , apartments: 1 , singlefamilys: 9 , condos: 0 , townhouses: 0 },
    {name: 'Indianapolis' , apartments: 0 , singlefamilys: 10 , condos: 0 , townhouses: 0 },
    {name: 'Miami' , apartments: 10 , singlefamilys: 0 , condos: 0 , townhouses: 0 },
    {name: 'Mesa' , apartments: 0 , singlefamilys: 10 , condos: 0 , townhouses: 0 },
    {name: 'Kansas City' , apartments: 4 , singlefamilys: 6 , condos: 0 , townhouses: 0 },
    {name: 'Portland' , apartments: 0 , singlefamilys: 10 , condos: 0 , townhouses: 0 },
    {name: 'Dallas' , apartments: 0 , singlefamilys: 7 , condos: 2 , townhouses: 1 },
    {name: 'Austin' , apartments: 10 , singlefamilys: 0 , condos: 0 , townhouses: 0 },
    {name: 'New York' , apartments: 4 , singlefamilys: 5 , condos: 1 , townhouses: 0 },
    {name: 'Wichita' , apartments: 1 , singlefamilys: 8 , condos: 1 , townhouses: 0 },
    {name: 'Minneapolis' , apartments: 6 , singlefamilys: 3 , condos: 0 , townhouses: 1 },
    {name: 'Detroit' , apartments: 1 , singlefamilys: 6 , condos: 1 , townhouses: 0 },
    {name: 'Omaha' , apartments: 0 , singlefamilys: 4 , condos: 6 , townhouses: 0 },
    {name: 'Jacksonville' , apartments: 0 , singlefamilys: 10 , condos: 0 , townhouses: 0 },
    {name: 'El Paso' , apartments: 3 , singlefamilys: 6 , condos: 0 , townhouses: 1 },
    {name: 'Oakland' , apartments: 2 , singlefamilys: 7 , condos: 0 , townhouses: 0 },
    {name: 'San Diego' , apartments: 0 , singlefamilys: 9 , condos: 0 , townhouses: 1 },
    {name: 'Fresno' , apartments: 0 , singlefamilys: 9 , condos: 1 , townhouses: 0 },
    {name: 'Oklahoma City' , apartments: 0 , singlefamilys: 10 , condos: 0 , townhouses: 0 },
    {name: 'San Jose' , apartments: 1 , singlefamilys: 6 , condos: 1 , townhouses: 2 },
    {name: 'Long Beach' , apartments: 10 , singlefamilys: 0 , condos: 0 , townhouses: 0 },
    {name: 'Phoenix' , apartments: 0 , singlefamilys: 9 , condos: 0 , townhouses: 1 },
    {name: 'Columbus' , apartments: 4 , singlefamilys: 6 , condos: 0 , townhouses: 0 },
    {name: 'San Francisco' , apartments: 0 , singlefamilys: 9 , condos: 1 , townhouses: 0 },
    {name: 'Denver' , apartments: 0 , singlefamilys: 10 , condos: 0 , townhouses: 0 },
    {name: 'Seattle' , apartments: 1 , singlefamilys: 9 , condos: 0 , townhouses: 0 },
    {name: 'Philadelphia' , apartments: 0 , singlefamilys: 7 , condos: 0 , townhouses: 0 },
    {name: 'Tulsa' , apartments: 0 , singlefamilys: 10 , condos: 0 , townhouses: 0 },
    {name: 'Sacramento' , apartments: 2 , singlefamilys: 8 , condos: 0 , townhouses: 0 },
    {name: 'Las Vegas' , apartments: 1 , singlefamilys: 8 , condos: 0 , townhouses: 1 },
    {name: 'Milwaukee' , apartments: 4 , singlefamilys: 4 , condos: 1 , townhouses: 0 },
    {name: 'Arlington' , apartments: 0 , singlefamilys: 10 , condos: 0 , townhouses: 0 },
    {name: 'Memphis' , apartments: 0 , singlefamilys: 10 , condos: 0 , townhouses: 0 },
    {name: 'Raleigh' , apartments: 0 , singlefamilys: 9 , condos: 0 , townhouses: 1 },
    {name: 'Tucson' , apartments: 0 , singlefamilys: 10 , condos: 0 , townhouses: 0 },
]

const ProviderApartmentTypeDistributionVisualization = () => {
    const [data, setData] = useState<any[]>([]);
    const [loaded, setLoaded] = useState(true);

    useEffect(() => {
        if (!loaded){
            const tempData: any[] = [];
        }
        else {
            setData(cached_data)
        }
    }, []);

    return (
        <Box>
            <Typography gutterBottom variant="subtitle1" component="div" pt={2}>
            Apartment Property Type Distribution by City
            </Typography>
            <Grid container justifyContent="center">
                {loaded ? (
                    <BarChart
                    width = {1000}
                    height = {400}
                    data = {data}
                    margin={{
                        top: 20,
                        right: 30,
                        left: 20,
                        bottom: 5,
                      }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" hide/>
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Bar dataKey="apartments" stackId="a" fill="#FD7E14" />
                    <Bar dataKey="singlefamilys" stackId="a" fill="#FDB814" />
                    <Bar dataKey="condos" stackId="a" fill="#93FD14" />
                    <Bar dataKey="townhouses" stackId="a" fill="#59FD14" />
                </BarChart>
                ) : (
                    <CircularProgress />
                )}
            </Grid>
        </Box>
    )
};

export default ProviderApartmentTypeDistributionVisualization;